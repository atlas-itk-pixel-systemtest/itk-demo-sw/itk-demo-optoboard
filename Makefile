build:
				git submodule update --init --recursive
				docker build -t itk-demo-optoboard . --progress plain
				# docker build -t itk-demo-optoboard-worker -f worker.Dockerfile . --progress plain
rebuild:
				docker build -t itk-demo-optoboard . --progress plain --no-cache
up:
				docker network inspect demi >/dev/null 2>&1 || docker network create demi
				docker-compose up -d --build --remove-orphans

down:
				docker-compose down

recompose: down up