import os
from connexion import FlaskApp
from connexion.exceptions import ProblemException
from flask_cors import CORS

def create_app():
    cwd = os.getcwd()

    print(cwd)

    app = FlaskApp(
        __name__,
        specification_dir="openapi",
        options={
            "swagger_ui": True,
            "swagger_url": "/ui",
        },
        server_args={
            "static_folder": cwd + "/../ui/build",
            "static_url_path": "",
            # 'instance_relative_config': True,
        },
    )

    app.add_error_handler(404, flask_error_response)  
    app.add_error_handler(501, flask_error_response)
    app.add_error_handler(ProblemException, connexion_error_response)

    # create the app
    flask_app = app.app
    CORS(flask_app)

    with app.app.app_context():
        app.add_api("openapi.yaml")

    flask_app.config.update(
        CELERY_BROKER_URL="amqp://guest@rabbitmq//",
        CELERY_RESULT_BACKEND="rpc://guest@rabbitmq//",
    )

    proxied = ReverseProxied(flask_app.wsgi_app, script_name="/optoboard/api/")
    flask_app.wsgi_app = proxied

    return app


def flask_error_response(error):
    dic = {"data": error.description, "status": error.code}
    return dic, error.code, {"content-type": "application/json"}


def connexion_error_response(error):
    dic = {"data": error.detail, "status": error.status}
    return dic, error.status, {"content-type": "application/json"}


class ReverseProxied:
    """Wrap the application in this middleware and configure the
    reverse proxy to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:

    location /proxied {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Forwarded-Path /proxied;
    }

    :param app: the WSGI application
    :param script_name: override the default script name (path)
    :param scheme: override the default scheme
    :param server: override the default server
    """

    def __init__(self, app, script_name=None, scheme=None, server=None):
        self.app = app
        self.script_name = script_name
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get("HTTP_X_FORWARDED_PATH", "") or self.script_name
        if script_name:
            environ["SCRIPT_NAME"] = "/" + script_name.lstrip("/")
            path_info = environ["PATH_INFO"]
            if path_info.startswith(script_name):
                environ["PATH_INFO_OLD"] = path_info
                environ["PATH_INFO"] = path_info[len(script_name) :]
        scheme = environ.get("HTTP_X_SCHEME", "") or self.scheme
        if scheme:
            environ["wsgi.url_scheme"] = scheme
        server = environ.get("HTTP_X_FORWARDED_SERVER", "") or self.server
        if server:
            environ["HTTP_HOST"] = server
        return self.app(environ, start_response)

# create the application
app = create_app()

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5009)
