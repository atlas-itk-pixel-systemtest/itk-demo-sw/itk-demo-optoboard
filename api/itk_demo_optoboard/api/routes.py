from connexion import request
from time import sleep
import logging
from itk_demo_optoboard.exceptions import _error_catcher
from celery.result import AsyncResult
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
logger.addHandler(ch)

def health():
    return _error_catcher(lambda: {"status": 200})


def healthCelery():
    return _error_catcher(
        healthCeleryTask.apply_async(queue="itk_demo_optoboard_queue").wait
    )


def longTaskCelery():
    return _error_catcher(
        longTaskCeleryTask.apply_async(queue="itk_demo_optoboard_queue_OB0").wait
    )

def loadOptoList():
    return _error_catcher(
        loadOptoListTask.apply_async(queue="itk_demo_optoboard_queue_OB0").wait
    )

def initOptoList():
    payload = request.get_json()
    opto_list = payload.get('optoList', {})  # Extract the 'optoList' part
    runkeys_config = payload.get('runkeys_config', {})  # Extract the 'runkeys_config' part
    
    return _error_catcher(
        initOptoListTask.apply_async(
            args=(opto_list,runkeys_config,), queue="itk_demo_optoboard_queue_OB0"
        ).wait
    )

def getOptoGUIConf():
    return _error_catcher(
        getOptoGUIConfTask.apply_async(queue="itk_demo_optoboard_queue_OB0").wait
    )

def resetGBCR():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        resetGBCRTask.apply_async(
            (optoboardPosition,),
            queue=queue,
        ).wait)

def resetVTRX():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        resetVTRXTask.apply_async(
            (optoboardPosition,),
            queue=queue,
        ).wait)


def modifyOptoGUIConf():
    payload = request.get_json()
    confKey = str(payload["confKey"])
    newConfiguration = str(payload["newConfiguration"])
    return _error_catcher(
        modifyOptoGUIConfTask.apply_async(
            (confKey, newConfiguration), queue="itk_demo_optoboard_queue_OB0"
        ).wait
    )


def writeRegister():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    register = str(payload["register"])
    newValue = payload["newValue"]
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        writeReadTask.apply_async(
            (optoboardPosition, device, register, newValue),
            queue=queue,
        ).wait
    )


def readRegister():
    payload = request.get_json()

    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    register = str(payload["register"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        readTask.apply_async(
            (optoboardPosition, device, register), queue=queue
        ).wait
    )


def configure():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    activeLpgbt = str(payload["activeLpgbt"])
    activeGbcr = str(payload["activeGbcr"])
    if activeLpgbt == "None":
        activeLpgbt = None
    if activeGbcr == "None":
        activeGbcr = None
    queue = "itk_demo_optoboard_queue_"+optoboardPosition
    return _error_catcher(
        configureTask.apply_async(
            (optoboardPosition, activeLpgbt, activeGbcr),
            queue=queue,
        ).wait
    )


def configureAll():
    payload = request.get_json()
    optoList = payload["optoList"]
    logger.info(optoList)
    tasks = []
    exit_codes = []
    replies = []
    summary = {"Summary": "Full box configuration failed!"}
    summary.update({key: "Configuration not attempted" for key in optoList})
    for optoboardPosition in optoList:
        queue = "itk_demo_optoboard_queue_" + optoboardPosition
        task = configureTask.apply_async((optoboardPosition,), queue=queue)
        tasks.append(task)

    # Retrieve results and handle errors
    for task in tasks:
        reply, exit_code = _error_catcher(AsyncResult(task.id).get)            
        exit_codes.append(exit_code)
        replies.append(reply)
    for i,x in enumerate(exit_codes):
        key = optoList[i]
        if "Configuration failed" in reply:
            summary[key] = "Reply: " + str(replies[i])
        else:
            summary[key] = "Reply: " + str(replies[i])
    logger.info(summary.values())
    if "Configuration failed" not in replies:
        summary["Summary"] = "Full box configuration completed!"        
        return summary, 200, {"content-type": "application/json"}
    else:            
        return (
            summary,
            500,
            {"content-type": "application/json"},
        )


""" def initializeOptoboard_wrapper():

    payload = request.get_json()
    optoserial = str(payload["optoserial"])
    config_path = str(payload["config_path"])
    vtrx_v = str(payload["vtrx_v"])
    flx_G = int(payload["flx_G"])
    flx_d = int(payload["flx_d"])
    woflxcore = bool(payload["woflxcore"])
    debug = bool(payload["debug"])
    testmode = bool(payload["testmode"])

    boolSerial, lpgbt_v, activeLpgbt, activeGbcr = initializeOptoboardTask.apply_async((optoserial, config_path, vtrx_v, flx_G, flx_d, woflxcore, debug, testmode), queue="itk_demo_optoboard_queue").wait()

    return {"boolSerial" : boolSerial, "lpgbt_v" : str(lpgbt_v), "activeLpgbt": activeLpgbt, "activeGbcr": activeGbcr}, 200, {'content-type': 'application/json'} """


""" def sendRegister_wrapper():
    payload = request.get_json()
    print(payload)

    device = payload['device']
    registerName = payload['registerName']
    fieldName = payload['settingName']
    newValue = payload['newValue']
    if fieldName == 'DPDATAPATTERN': 
        newValue = int(newValue,2)
    else: newValue = int(newValue)

    read_back = writeReadTask.apply_async((device, registerName.upper(), fieldName.upper(), newValue), queue="itk_demo_optoboard_queue").wait()
    #read_back = write_read(device, registerName.upper(), fieldName.upper(), newValue)

    return str(f'Read-back value {read_back}'), 200, {'content-type': 'application/json'} """


def optoStatus():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        optoStatusTask.apply_async(
            (optoboardPosition,), queue=queue
        ).wait
    )
    
def resetSlaveClock():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    slave_name = str(payload["slave_name"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        resetSlaveClockTask.apply_async(
            (optoboardPosition, device, slave_name), queue=queue,
        ).wait
    )

def phaseScan():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    eprx_group = int(payload["eprx_group"])
    bertmeastime = int(payload["bertmeastime"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        phaseScanTask.apply_async(
            (optoboardPosition, device,eprx_group, bertmeastime),
            queue=queue,
        ).wait
    )
        

'''def softErrorCounter():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    optical_link = str(payload["optical_link"])
    felix_device = int(payload["felix_device"])
    egroup = int(payload["egroup"])
    meastime = int(payload["meastime"])
    API = bool(int(payload["API"]))
    url = str(payload["url"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        softErrorCounterTask.apply_async(
            (optoboardPosition, optical_link, felix_device, egroup, meastime, API, url),
            queue=queue,
        ).wait
    )'''
    
        
def softErrorCounter():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    dev_olink_egrp = list(payload["dev_olink_egrp"])
    meastime = int(payload["meastime"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        softErrorCounterTask.apply_async((optoboardPosition,dev_olink_egrp,meastime),queue=queue).wait
    )


def softErrorScan():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    dev_olink_egrp = list(payload["dev_olink_egrp"])
    gbcr_name = str(payload["gbcr_name"])
    meastime = int(payload["meastime"])
    filename = str(payload["filename"])
    plot = bool(int(payload["plot"]))
    HFmin = int(payload["HFmin"])
    HFmax = int(payload["HFmax"])
    MFmin = int(payload["MFmin"])
    MFmax = int(payload["MFmax"])
    jump = int(payload["jump"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        softErrorScanTask.apply_async(
            (optoboardPosition, dev_olink_egrp, gbcr_name, meastime, filename, plot, HFmin, HFmax, MFmin, MFmax, jump),
            queue=queue,
        ).wait
    )
        


def BERT():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    channel = int(payload["channel"])
    meastime = int(payload["meastime"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        bertTask.apply_async(
            (optoboardPosition, device, channel, meastime),
            queue=queue,
        ).wait
    )

def swapPolarity():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    link = int(payload["link"])
    TxRx = str(payload["TxRx"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        swapPolarityTask.apply_async(
            (optoboardPosition, device, TxRx, link), queue=queue,
        ).wait
    )


def setGBCREqualization():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    channel = int(payload["channel"])
    mFreq = int(payload["mFreq"])
    hFreq = int(payload["hFreq"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        setGCBREqualizationTask.apply_async(
            (optoboardPosition, device, channel, mFreq, hFreq),
            queue=queue,
        ).wait
    )


def setLpgbtPhase():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    phaseMode = str(payload["phaseMode"])
    group = int(payload["group"])
    phase = int(payload["phase"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        setLpgbtPhaseTask.apply_async(
            (optoboardPosition, device, phaseMode, group, phase),
            queue=queue,
        ).wait
    )
    
def configureI2CController():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        configureI2CControllerTask.apply_async(
            (optoboardPosition, device),
            queue=queue,
        ).wait
    )
    
def dumpCustomRegConfig():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    filename = str(payload["filename"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition    
    return _error_catcher(
        dumpCustomRegConfigTask.apply_async(
            (optoboardPosition, filename),
            queue=queue,
        ).wait
    )

def readLog():
    return _error_catcher(
        readLogTask.apply_async(queue="itk_demo_optoboard_queue").wait
    )
    
def readLpgbtSupplyVoltage():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    monitor_channel = int(payload["monitor_channel"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        readSupplyVoltageTask.apply_async((optoboardPosition,device,monitor_channel),queue=queue).wait
    )
    
def eyeMonitorDiagram():
    payload = request.get_json()
    optoboardPosition = str(payload["optoboardPosition"])
    device = str(payload["device"])
    meastime = int(payload["meastime"])
    filename = str(payload["filename"])
    queue = "itk_demo_optoboard_queue_" + optoboardPosition
    return _error_catcher(
        eyeMonitorDiagramTask.apply_async((optoboardPosition,device,meastime,filename),queue=queue).wait
    )   

# from itk_demo_optoboard.wsgi import celery
from itk_demo_optoboard.worker.celeryTasks import *
