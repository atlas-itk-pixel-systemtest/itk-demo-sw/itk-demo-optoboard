from rcf_response import Error

from optoboard_felix.driver.log import ch, logger

def _error_catcher(func):
    try:
        response = func()
    except NameError as err:
        err = Error(400, "Name Error", err.__class__.__name__ + " " + str(err))
        logger.error(err.to_conn_response())
        return err.to_conn_response()
    except ValueError as err:
        err = Error(400, "Value Error", err.__class__.__name__ + " " + str(err))
        logger.error(err.to_conn_response())
        return err.to_conn_response()
    except AttributeError as err:
        err = Error(400, "Device Error", err.__class__.__name__ + " " + str(err))
        logger.error(err.to_conn_response())
        return err.to_conn_response()
    # except (CustomError, api_CustomError) as error:
    #     err = Error(error.id, error.title, error.text)
    #     return err.to_conn_response()
    except FileNotFoundError as err:
        err = Error(500, "File Not Found", err.__class__.__name__ + " " + str(err))
        logger.error(err.to_conn_response())
        return err.to_conn_response()
    except Exception as err:
        err = Error(503, "Service Unavailable", err.__class__.__name__ + " " + str(err))
        logger.error(err.to_conn_response())
        return err.to_conn_response()
    return response, 200 #, {"content-type": "application/json"}

#THIS DOESN'T WORK IN THIS CONFIGURATION FOR FUNCTIONS WHERE ARGUMENTS PROVIDED INTERNALLY
def task_error_catcher(func,*args):
    try:    
        response = func(*args)
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
