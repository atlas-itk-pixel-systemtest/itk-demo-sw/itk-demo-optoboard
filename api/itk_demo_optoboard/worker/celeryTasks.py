import json
from json import JSONDecodeError
from time import sleep
import traceback
import os
import sys
from collections import OrderedDict
from requests import get
import requests
from requests.exceptions import ConnectionError, RequestException
from pyconfigdb.configdb import ConfigDB
from itk_demo_optoboard.worker.celery import celery
from celery.signals import worker_ready, worker_init
from optoboard_felix.additional_driver.RepoConfig import construct_config_files

# sys.path.append(os.getcwd() + "itk_demo_optoboard/optoboard_felix")



from optoboard_felix.driver.CommWrapper import CommWrapper
# from optoboard_felix.driver.components import components
from optoboard_felix.driver.log import ch, logger
from optoboard_felix.driver.Optoboard import Optoboard
from optoboard_felix.driver.Lpgbt import Lpgbt
from optoboard_felix.driver.Gbcr import Gbcr
from optoboard_felix.InitOpto import InitOpto

### check celery status

@celery.task()
def healthCeleryTask():
    logger.info("Celery is running! The connection to the microservice backend works!")
    return {"status": 200}

@celery.task()
def longTaskCeleryTask():
    logger.info("Celery is running! The connection to the microservice backend works!")
    logger.info("Sleeping for 10 seconds...")
    sleep(20)
    return {"status": 200}

@worker_ready.connect
def run_startup_task(**kwargs):
    logger.info("Executing run_startup_task at worker_ready signal...")   
    #Send task to the worker, don't wait for a reply
    loadOptoListTask.apply_async(queue="itk_demo_optoboard_queue")
    logger.info("run_startup_task executed!")
    
    
## initialize optoboard object after config file modification/startup

@celery.task()
def loadOptoListTask():
    dict_config, runkeys_config = getOptoGUIConfTask()
    optoGUIConf = dict_config["optoGUIConf"]
    defaultConfig = dict_config["defaultConfig"]
    global optoboard_dic, opto_info_dic
    optoboard_dic = {}
    opto_info_dic = {}
    optoboard_dic, opto_info_dic, opto = InitOpto.InitOpto(optoListConfig=optoGUIConf[defaultConfig], runkeys_config=runkeys_config[defaultConfig])
    logger.info("Configuration loaded!")
    if os.environ.get("AUTO_CONFIG_OPTOs") == 1:
        logger.info("Configuring all optoboards...")
        try:
            configureAllTask()
        except:
            logger.error("The configureAllTask failed! Check if felixcore/felixstar is running!")
    return opto_info_dic, {"optoGUIConf": optoGUIConf, "defaultConfig": defaultConfig}



### initialize the optoboard object

@celery.task()
def initOptoListTask(opto_dict_main, runkeys_config):
    global optoboard_dic, opto_info_dic
    optoboard_dic = {}
    opto_info_dic = {}
    logger.info(f"opto_dict_main: {opto_dict_main}")
    optoboard_dic, opto_info_dic, opto = InitOpto.InitOpto(optoListConfig=opto_dict_main, runkeys_config=runkeys_config)
    return opto_info_dic


@celery.task()
def getOptoGUIConfTask():
    if os.environ.get("OB_USE_CONFIGDB") == "1":
        defaultConfig = os.environ.get("RUNKEY_NAME")
        optoGUIConf, runkeys_config  = construct_config_files()
        logger.info("Default config: " + str(defaultConfig))
        logger.info("Opto GUI config: " + str(optoGUIConf))
        logger.info("Configuration loaded!")
        
    else:
        logger.info("Loading configuration from file...")
        runkeys_config = {}
        try:
            with open("/config/ConfigGUI.json") as f:
                config_json = json.load(f)
                defaultConfig = config_json["defaultConfig"]
                logger.info("Default config: " + str(defaultConfig))
                optoGUIConf = {x: config_json[x] for x in config_json if x != "defaultConfig"}
                logger.info("Opto GUI config: " + str(optoGUIConf))
                runkeys_config[defaultConfig] = {"None": "None"}    
        except:
            logger.error("The configuration file could not be loaded! The config file should be located at /config/ConfigGUI.json. The worker will not automatically create an opto object.")
            raise FileNotFoundError("/config/ConfigGUI.json was not found!")

    return {"optoGUIConf": optoGUIConf, "defaultConfig": defaultConfig}, runkeys_config



@celery.task()
def modifyOptoGUIConfTask(confKey, newConfiguration):
    if confKey != "defaultConfig":
        newConfiguration = json.loads(newConfiguration)

    try:
        with open("/config/ConfigGUI.json", "r+") as f:
            config_json = json.load(f)
            config_json[confKey] = newConfiguration

            defaultConfig = config_json["defaultConfig"]
            optoGUIConf = {x: config_json[x] for x in config_json if x != "defaultConfig"}

            # logger.warn(json.dumps(config_json, indent = 4))
            f.seek(0)
            f.truncate(0)
            json.dump(config_json, f, indent=2)
    except:
        raise FileNotFoundError("ConfigGUI.json not found!")

    return {"optoGUIConf": optoGUIConf, "defaultConfig": defaultConfig}


@celery.task()
def writeReadTask(optoboardPosition, device, register, newValue):
    try:
        newValue = int(newValue)
    except: 
        raise ValueError("Conversion of newValue to int failed!")

    if register == "":
        raise NameError("Empty register name!")
    
    registerName = str(register).split(" ")[0].upper()
    try:
        subregisterName = str(register).split(" ")[1].upper()
    except:
        subregisterName = None

    replyTask = eval(
        'optoboard_dic["' + optoboardPosition + '"].' + device
    ).write_read(registerName, subregisterName, newValue)
    
    logger.info("Read-back value for " + register + " : " + str(replyTask))
    return replyTask

    # except Exception as inst:
    #    if 'Resourses locked: felixcore is probably running!' in inst.args:
    #        RequestStatus = 404
    #    else:
    #        RequestStatus = 500
    # , RequestStatus


@celery.task()
def readTask(optoboardPosition, device, register):

    if register == "":
        raise NameError("Empty register name!")
    
    registerName = str(register).split(" ")[0].upper()
    try:
        subregisterName = str(register).split(" ")[1].upper()
    except:
        subregisterName = None

    try:
        replyTasks = eval('optoboard_dic["' + optoboardPosition + '"].' + device).read(registerName, subregisterName)
        logger.info("Value for " + register + " : " + str(replyTasks))
        return replyTasks
    except Exception as e:
        logger.error("Could not read register: " + str(e))

### optoboard status

@celery.task()
def optoStatusTask(optoboardPosition):
    full_dic = optoboard_dic[optoboardPosition].opto_status()
    status = {"lpgbt1": 0, "lpgbt2": 0, "lpgbt3": 0, "lpgbt4": 0}
    for key, device_dic in full_dic.items():
        if key[:-1] == "lpgbt":
            if device_dic["PUSM_ready"] is True:
                status[key] = 1
    return status

### reset lpgbt slave clocks

@celery.task()
def resetSlaveClockTask(optoboardPosition, device, slave_name):
    eval('optoboard_dic["' + optoboardPosition + '"].' + device).reset_slave_clock(slave_name)
    if device == "lpgbt1" and slave_name != "gbcr":
        return "Reset of " + slave_name + " clock successful"
    elif device == "lpgbt1" and slave_name == "gbcr":
        return "Reset of " + slave_name + " of master lpGBT clock successful"
    else:
        return "Reset of " + slave_name + " clock of " + device + " successful"
    
### configuration task

@celery.task()
def configureTask(optoboardPosition, activeLpgbt=None, activeGbcr=None):
    logger.info("Starting configuration of optoboard in position " + optoboardPosition + "...")
    try:
        optoboard_dic[optoboardPosition].configure(activeLpgbt, activeGbcr)
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Configuration failed"
    return "Configuration completed"


@celery.task()
def configureAllTask():
    for pos, opto in optoboard_dic.items():
        try:
            opto.configure()
        except Exception as e:
            logger.error("Task terminated due to exception: " + str(e))
            return "Configuration failed"
    return "Configuration completed"


@celery.task()
def phaseScanTask(optoboardPosition, device,eprx_group, bertmeastime):
    phaseScan_result = eval('optoboard_dic["' + optoboardPosition + '"].' + device).bert_by_phase(eprx_group, bertmeastime, 6)
    return phaseScan_result


'''@celery.task()
def softErrorCounterTask(optoboardPosition, optical_link, felix_device, egroup, meastime, API=True, url="http://felix:8000"):
    softErrorCounter_result = optoboard_dic[optoboardPosition].softErrorCounter(optical_link, felix_device, egroup, meastime, API, url)
    return softErrorCounter_result'''


@celery.task()
def softErrorScanTask(optoboardPosition, dev_olink_egrp, gbcr_name, meastime, filename="softErrorScan_result", plot=False, HFmin=0, HFmax=15, MFmin=0, MFmax=15, jump=1):
    try:
        softErrorScan_result = optoboard_dic[optoboardPosition].softErrorScan(dev_olink_egrp, gbcr_name, meastime, filename, plot, HFmin, HFmax, MFmin, MFmax, jump)
        return softErrorScan_result
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not perform soft error scan!"
    return "Soft error scan files created!"

@celery.task()
def bertTask(optoboardPosition, device, channel, meastime):
    BERT_result = eval('optoboard_dic["' + optoboardPosition + '"].' + device).bert(channel, meastime, 6)
    return BERT_result

@celery.task()
def swapPolarityTask(optoboardPosition, device, TxRx, link):
    if TxRx == "Tx":
        eval('optoboard_dic["' + optoboardPosition + '"].' + device).tx_polarity_swap(link)
        return "Tx polarity swapped"
        
    if TxRx == "Rx":
        eval('optoboard_dic["' + optoboardPosition + '"].' + device).rx_polarity_swap(link)
        return "Rx polarity swapped"
        
@celery.task()
def setGCBREqualizationTask(optoboardPosition, device, channel, mfreq, hfreq):
    eval('optoboard_dic["' + optoboardPosition + '"].' + device).set_equalizer(channel, mfreq, hfreq)
    return "GBCR equalization updated"

@celery.task()
def setLpgbtPhaseTask(optoboardPosition, device, phaseMode, group, phase):
    eval('optoboard_dic["' + optoboardPosition + '"].' + device).set_phase_mode(phaseMode, group, phase)
    return "Phase mode updated"

@celery.task()
def readLogTask():
    workspace = os.environ.get("WORKSPACE")
    if workspace is not None:
        file_name = str(workspace) + '/optoboard.log'
    else:
        file_name = 'optoboard.log'
    with open(file_name, "r") as f:
        log = f.read()
    return log

@celery.task()
def configureI2CControllerTask(optoboardPosition, device):
    try:
        eval('optoboard_dic["' + optoboardPosition + '"].' + device).configure_I2C_controller()
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not configure I2C controller!"
    return "Configuration of I2C controller completed"

@celery.task()
def dumpCustomRegConfigTask(optoboardPosition,filename):
    try:
        optoboard_dic[optoboardPosition].dump_opto_config(filename)
    except Exception as e:
            logger.error("Task terminated due to exception: " + str(e))
            return "Could not create configuration file!"
    return "Finished!"

@celery.task()
def softErrorCounterTask(optoboardPosition,dev_olink_egrp,meastime):
    try:
        softerrors = optoboard_dic[optoboardPosition].softErrorCounter(dev_olink_egrp,meastime)
        return softerrors
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not perform parallel soft error test!"

@celery.task()
def readSupplyVoltageTask(optoboardPosition, device, monitor_channel):
    try:
        voltage = eval('optoboard_dic["' + optoboardPosition + '"].' + device).read_supply_voltage(monitor_channel)
        return voltage
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not read supply voltage of " + device + "!"
    
@celery.task()
def eyeMonitorDiagramTask(optoboardPosition, device, meastime, filename):
    try:
        emd = eval('optoboard_dic["' + optoboardPosition + '"].' + device).eye_opening_monitor(meastime,filename)
        return emd
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not produce eye monitior diagram for " + device + "!"


@celery.task()
def resetGBCRTask(optoboardPosition):
    try:
        eval('optoboard_dic["' + optoboardPosition + '"].lpgbt1').send_pulse_to_GBCR_reset()
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not reset the GBCR!"
    return "Reset of the GBCR completed"

@celery.task()
def resetVTRXTask(optoboardPosition):
    try:
        eval('optoboard_dic["' + optoboardPosition + '"].lpgbt1').send_pulse_to_VTRX_reset()
    except Exception as e:
        logger.error("Task terminated due to exception: " + str(e))
        return "Could not reset the VTRX!"
    return "Reset of the VTRX completed"