from celery import Celery
from kombu import Queue
def make_celery():
    celery = Celery(
        __name__,
        backend="rpc://guest@rabbitmq//",
        broker="amqp://guest@rabbitmq//",
        include=["itk_demo_optoboard.worker.celeryTasks"],
    )
    queue_names = [ 'itk_demo_optoboard_queue','itk_demo_optoboard_queue_OB0','itk_demo_optoboard_queue_OB1', 'itk_demo_optoboard_queue_OB2','itk_demo_optoboard_queue_OB3','itk_demo_optoboard_queue_OB4','itk_demo_optoboard_queue_OB5','itk_demo_optoboard_queue_OB6','itk_demo_optoboard_queue_OB7']
    task_queues = []
    for queue_name in queue_names:
        task_queues.append(Queue(queue_name, routing_key=queue_name))
    celery.conf.task_queues = task_queues
    celery.conf.task_default_queue = "itk_demo_optoboard_queue"
    celery.conf.task_create_missing_queues = False
    celery.conf.worker_concurrency = len(queue_names)
    return celery


def init_celery(celery, app):

    celery.conf.update(app.config)

    
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask

    return celery

celery = make_celery()

if __name__ == "__main__":
    from itk_demo_optoboard.api.wsgi import app
    init_celery(celery, app)