import json
from requests import post

DB_API_URL = "http://localhost:5001/"

def main():

    #open config-file and saves text as string
    with open('optoboard_GUI_default_config.json') as file:
        # opto_text = json.loads(file)
        opto_text = file.read()
    
    #create dictionary with necessary properties for set-endpoint
    name = "optoboard_GUI_default_config"
    dic = {"name":name, "data":opto_text, "type":"optoboard"}

    #execute set endpoint
   # execute(dic, "set", "component")

    #create dictionary with necessary properties for get-endpoint
    dic = {"name":name, "id":1}

    #execute get endpoint, save response
    response_dic = execute(dic, "get", "component")
    
    config_file = json.loads(response_dic["data"].replace("'","\""))

    print(config_file["Optoboard"])
    print(type(config_file))

    # this value will be changed
    config_file["Optoboard"]["version"] = 200

    dic = {"name":name, "data":"test", "type":"optoboard"}
    dic["data"] = json.dumps(config_file)

    # print(dic)

    execute(dic, "update", "component")

    dic = {"name":name, "id":1}

    #execute get endpoint, save response
    response_dic = execute(dic, "get", "component")
    
    config_file = json.loads(response_dic["data"].replace("'","\""))

    print(config_file["Optoboard"])
    print(type(config_file))




def execute(con, method, object = ""):
    """function to post-dictionary to configdb server
        prints the status of the response to console

    Parameters
    ----------
    dic : dictionary
        python dictionary containing the necessary properties (see documentation)
    method : str
        method that should be accessed (e.g. get)
    object: str
        table that should be accessed (e.g. component), empty for get_all endpoint

    Returns
    -------
    dictionary
        Dictionary containing the properties received from the HTTP-response
    """

    headers = {'content-type': 'application/json', 'accept': 'application/json'}

    if object != "":
        res = post(DB_API_URL + object + '/' + method, data=json.dumps(con), headers=headers)
    else:
        res = post(DB_API_URL + method, data=json.dumps(con), headers=headers)

    if res.ok:
        #loads response-json to python dictionary
        dic = json.loads(res.content)
        statustext = statuscode_text(dic["statuscode"], method, table=object)
        print("%s method %s: %s" %(method, dic["id"], statustext))
        return dic
    else:
        print("HTTP Post failed.")

def statuscode_text(statuscode, method = "", name = "", table = ""):
    """function to get statustext by statuscode

    Parameters
    ----------
    statuscode : int
        statuscode for which the statustext should be returned
    method : str
        method that was accessed (optional: puts additional information in text)
    name: str
        name of the dataset that was accessed (optional: puts additional information in text)
    type: 
        table that was accessed (optional: puts additional information in text)
    Returns
    -------
    string
        statustext
    """

    statuscode_dict = {
        1: "Successfully executed",
        2: "The sent type does not exist for the specified table, please check the type property and try again.",
        3: "The requested table does not exist, please check the table property and try again.",
        100: "Item %s was not found in %s -table" %(name, table),
        101: "Name %s already in use" %(name),
        103: "Task failed, one or more of the given datasets do not exist. See logging for details and try again.",
        104: "Task failed, one or more of the given datasets were already in the runkey. See logging for details and try again.",
        105: "The component %s could not be deleted, because it is part of a runkey." %(name),
        200: "The method %s expects a different kind of transporter-object" %(method),
        201: "The method %s does not exist" %(method),
        202: "Cannot execute empty Config-object. Please make sure that methode, type, name and version are set.",
        400: "The %s-method expects different format of data. \
            Please refer to the documentation for the %s-method for additional information." %(method, method),
        404: "Method %s not found, Please refer to the documentation for an overview of available methods" %(method),
        500: "Internal server error, please contact the admin and report this problem."
    }
    return statuscode_dict[statuscode]

if __name__ == '__main__':
    main()
