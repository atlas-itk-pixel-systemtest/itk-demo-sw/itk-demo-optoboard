#!/bin/bash

HOME_DIR="$( pwd )"
#FELIX_DIR=$1

#poetry shell
#cd $FELIX_DIR
#echo "      cd $FELIX_DIR"
#echo "      $HOME_DIR"
#export REGMAP_VERSION=0x0500
#source cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt
#cd $HOME_DIR
poetry run celery -A wsgi.celery worker -c 1 -l info 
echo
echo "      poetry run celery flower --broker=amqp://$q --port=5555"
echo
