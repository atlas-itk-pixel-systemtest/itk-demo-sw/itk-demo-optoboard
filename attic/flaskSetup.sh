#!/bin/bash

HOME_DIR="$( pwd )"
#FELIX_DIR=$1

#poetry shell
#cd $FELIX_DIR
#echo "      cd $FELIX_DIR"
#echo "      $HOME_DIR"
#export REGMAP_VERSION=0x0500
#source cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt
cd $HOME_DIR/ui
poetry run npm run build 
cd ..
poetry run bash bin/run.sh
###### alternative to the previous 4 lines ######
#poetry run flask run
