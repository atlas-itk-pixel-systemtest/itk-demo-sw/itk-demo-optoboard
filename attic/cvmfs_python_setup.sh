# instead of 'demi python install' do:

# 1. setup CVMFS compatible venv

git clone https://github.com/matthewfeickert/cvmfs-venv.git
. cvmfs-venv/atlas_setup.sh

# 2. use pip to install dependencies

python -m pip install -U --extra-index-url https://gitlab.cern.ch/api/v4/projects/123904/packages/pypi/simple/  .

