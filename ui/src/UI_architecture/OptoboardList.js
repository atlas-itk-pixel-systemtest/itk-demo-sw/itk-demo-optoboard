import React, { useState, useEffect } from "react";

import {
  Bullseye,
  FlexItem, 
  Spinner,
} from "@patternfly/react-core";

import CogIcon from "@patternfly/react-icons/dist/esm/icons/cog-icon";
import PlusCircleIcon from "@patternfly/react-icons/dist/esm/icons/plus-circle-icon";
import InfoCircleIcon from "@patternfly/react-icons/dist/esm/icons/info-circle-icon";

import { getPostBody, checkResponse } from "../utils/utility-functions";
import { SingleOptoPanel } from "./SingleOptoPanel";

export function OptoboardList({ url, DefaultConfig, Configurations, ConfigFile }) {
  const [optoList, setoptoList] = useState({});
  const [optoListInfo, setoptoListInfo] = useState({});
  const [loadingData, setloadingData] = useState(true);
  const [isDrawerExpanded, setisDrawerExpanded] = useState(false);

  useEffect(() => {
    setloadingData(true);
    if (Configurations === undefined) return;
    if (!DefaultConfig in Configurations) return;
    console.log("Asking for data!");
    fetch(
      `${url}/initOptoList`,
      getPostBody({ optoList: Configurations[DefaultConfig], runkeys_config: ConfigFile[DefaultConfig] })
    )
      .then((response) => checkResponse(response))
      .then((data) => {
        setoptoListInfo(data);
        setoptoList(Configurations[DefaultConfig]);
        setloadingData(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [DefaultConfig, url]);

  if (loadingData) {
    return (
      <FlexItem key={"optoInListLoad"}>
        <Bullseye>
          <Spinner
            isSVG
            diameter="120px"
            aria-label="spinner for loading data"
          />
        </Bullseye>
      </FlexItem>
    );
  } else {
    return Object.keys(optoList).map((position, index) => {
      return (
        <SingleOptoPanel
        url= {url}
        position = {position}
        index = {index} key = {index}
        optoListInfo = {optoListInfo}
        optoList = {optoList}
        />
       );
    });
  }
}
