import React, { useState, useEffect } from "react";
import { OptoboardList } from "./OptoboardList";
import { FooterComponentLogViewer } from "../OptoboardTools/FooterComponentLogViewer";

import {
  Button,
  Flex,
  FlexItem,
  Modal,
  ModalVariant,
  Text,
  TextContent,
  TextVariants,
  Panel,
  PanelMain,
  PanelMainBody,
  Page,
  FormSelect,
  FormSelectOption,
  NotificationDrawer,
  NotificationDrawerHeader,
  NotificationDrawerBody,
  NotificationDrawerList,
  NotificationDrawerListItem,
  NotificationDrawerListItemBody,
  NotificationDrawerListItemHeader,
  TextInput,
} from "@patternfly/react-core";
import { CodeEditor, Language } from "@patternfly/react-code-editor";

import {
  useConfigureValue
} from '@itk-demo-sw/hooks'

import {
  generalRequest
} from '@itk-demo-sw/utility-functions'

import { getPostBody, checkResponse } from "../utils/utility-functions";

export function OptoboardGUI() {
  const [DefaultConfig, setDefaultConfig] = useState("");
  const [ConfigFile, setConfigFile] = useState({});
  const [Configurations, setConfigurations] = useState({});
  const [DefaultConfigChange, setDefaultConfigChange] = useState(
    "Add New Configuration"
  );
  const [isOpenConfig, setisOpenConfig] = useState(false);
  const [isNotDrawerOpen, setisNotDrawerOpen] = useState(false);
  const [newConfigName, setnewConfigName] = useState("myConfigName");
  const [newDefault, setnewDefault] = useState("Choose Configuration");
  const [modalText, setmodalText] = useState("");

  const [ConfigurationOptions, setConfigurationOptions] = useState([]);

  const [ConfigurationOptions2Change, setConfigurationOptions2Change] =
    useState([]);

  const [loadingConfig, setloadingConfig] = useState(true);
  const [loadingString, setLoadingString] = useState("");
  const [optoConfigResult, setoptoConfigResult] = useState("primary");
  const [configureAllStatus, setconfigureAllStatus] = useState("primary");
  const [isConfiguring, setisConfiguring] = useState(false);
  
  const [isDangerHealthBool, setisDangerHealthBool] = useState(false);
  const [isDangerHealthCeleryBool, setisDangerHealthCeleryBool] = useState(false);
  const os = require('os'); 
  const url = useConfigureValue('/config', 'urlKey', "http://" + os.hostname() + ':5009', 'backendUrl');
  const runkeyUrl =  useConfigureValue('/config', 'runkeyUiUrlKey', "", '');
  let usingConfigDb = false;
  generalRequest('/config').then(data => {
    usingConfigDb = data["useConfigDb"] == "1";
  }
  );

  const [Log, setLog] = React.useState("");

  console.log('Url: ' + url)
  function getConfigGUI(url) {
    setloadingConfig(true);
    fetch(`${url}/getOptoGUIConf`)
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log("getOptoGUIConf");
        setDefaultConfig(data[0].defaultConfig);
        setConfigurations(data[0].optoGUIConf);
        setConfigFile(data[1]);
        setloadingConfig(false);
        console.log("getOptoGUIConf in useeffect");
        setLoadingString("");
      })
      .catch((err) => {
        console.log(err);
        setLoadingString("Could not retrieve the file ConfigGUI.json. Please check if the backend containers are running, if the file is properly mounted in the worker container in /config and then refresh the page.");
      });
  }

  useEffect(() => {
    getConfigGUI(url);
  }, [url]);

  function editConfig(confKey, newConfiguration, url) {
    console.log({ confKey: confKey, newConfiguration: newConfiguration });
    fetch(
      `${url}/modifyOptoGUIConf`,
      getPostBody({ confKey: confKey, newConfiguration: newConfiguration })
    )
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        // setisOpenConfig(false);
        getConfigGUI(url);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  useEffect(() => {
    setDefaultConfig(DefaultConfig);
    let X = [];
    let Y = [];
    for (var key in Configurations) {
      X.push({ value: key, label: key, disabled: false });
      Y.push({
        value: key,
        label: key,
        disabled: false,
      });
    }
    setConfigurationOptions([
      ...[
        {
          value: "Choose Configuration",
          label: "Choose Configuration",
          disabled: true,
        },
      ],
      ...X,
    ]);
    setConfigurationOptions2Change([
      ...[
        {
          value: "Add New Configuration",
          label: "Add New Configuration",
          disabled: false,
        },
      ],
      ...Y,
    ]);
  }, [Configurations]);

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     console.log("Periodic effect");
  //     fetch(`${url}/health`)
  //       .then((response) => checkResponse(response))
  //       .then((data) => {
  //         console.log(data);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //       });
  //   }, 200000);
  //   return () => {
  //     clearInterval(interval);
  //   };
  // }, [url]);

  useEffect(() => {
    const interval = setInterval(() => {
      console.log("Fetching logs");
      fetch(`${url}/readLog`)
        .then((response) => checkResponse(response))
        .then((data) => {
          setLog(data);
        })
        .catch((err) => {
          setLog("Internal Error while trying to fetch logs using " + url + "/readLog");
          console.log(err);
        });
    }, 2000);
    return () => {
      clearInterval(interval);
    };
  }, [url]);


  useEffect(() => {
    DefaultConfigChange == "Edit Configuration"
      ? setmodalText("")
      : setmodalText(
          JSON.stringify(Configurations[DefaultConfigChange], null, 2)
        );
  }, [DefaultConfigChange]);

  const notificationDrawer = (
    <NotificationDrawer>
      <NotificationDrawerHeader> </NotificationDrawerHeader>
      <NotificationDrawerBody>
        <NotificationDrawerList>
          <NotificationDrawerListItem variant="info">
            <NotificationDrawerListItemHeader
              variant="info"
              title="Unread info notification title"
              srTitle="Info notification:"
            ></NotificationDrawerListItemHeader>
            <NotificationDrawerListItemBody timestamp="5 minutes ago">
              This is an info notification description.
            </NotificationDrawerListItemBody>
          </NotificationDrawerListItem>
        </NotificationDrawerList>
      </NotificationDrawerBody>
    </NotificationDrawer>
  );

  if (loadingConfig) {
    return (
      <React.Fragment>
        <Page>
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <TextContent>
                  <Text component={TextVariants.h1}>Optoboard GUI</Text>
                </TextContent>
              </PanelMainBody>
            </PanelMain>
          </Panel>
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <Flex>
                  <FlexItem key={"optoInListLoad"}>
                    <Text component={TextVariants.h4}>{loadingString}</Text>
                    {/* <Bullseye>
                      <Spinner
                        isSVG
                        diameter="120px"
                        aria-label="spinnerforloadingconfig"
                      />
                    </Bullseye> */}
                  </FlexItem>
                </Flex>
              </PanelMainBody>
            </PanelMain>
          </Panel>
        </Page>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Page
          notificationDrawer={notificationDrawer}
          isNotificationDrawerExpanded={isNotDrawerOpen}
        >
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <TextContent>
                  <Text component={TextVariants.h1}>Optoboard GUI</Text>
                </TextContent>
              </PanelMainBody>
            </PanelMain>
          </Panel>
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <Flex>
                  <FlexItem key={"configureAll"}>
                    <Button
                      spinnerAriaValueText={
                        isConfiguring ? "ConfiguringAll" : undefined
                      }
                      isLoading={isConfiguring}
                      variant={configureAllStatus}
                      onClick={() => {
                        setisConfiguring(true);
                        console.log(Object.keys(Configurations[DefaultConfig]));
                        fetch(`${url}/configureAll`, getPostBody({"optoList": Object.keys(Configurations[DefaultConfig])}))
                          .then((response) => checkResponse(response))
                          .then((data) => {
                            console.log(data);
                            setconfigureAllStatus("primary");
                          })
                          .catch((err) => {
                            setconfigureAllStatus("danger");
                            console.log(err);
                          })
                          .finally(() => {
                            setisConfiguring(false);
                          });
                      }}
                    >
                      <h3> Configure all Optoboards </h3>
                    </Button>
                  </FlexItem>
                  <FlexItem key={"add_configuration"}>
                    <Button
                      variant="secondary"
                      onClick={() => {if (usingConfigDb) {
                        window.open(runkeyUrl, '_blank');}
                    else {
                      setisOpenConfig(!isOpenConfig)
                    }
                     }
                    }
                    >
                      <h3> Edit GUI Configuration </h3>
                    </Button>
                  </FlexItem>
                  <FlexItem>
                    <FormSelect
                      value={DefaultConfig}
                      onChange={(X) => {
                        setDefaultConfig(X);
                      }}
                      aria-label="FormSelect Input"
                    >
                      {ConfigurationOptions.map((option, index) => (
                        <FormSelectOption
                          isDisabled={option.disabled}
                          key={index}
                          value={option.value}
                          label={option.label}
                        />
                      ))}
                    </FormSelect>
                  </FlexItem>
                  <FlexItem key={"add configuration"}>
                    <Button
                      variant="link"
                      isDanger={isDangerHealthBool}
                      onClick={() => {
                        fetch(`${url}/health`)
                          .then((response) => checkResponse(response))
                          .then((data) => {
                            setisDangerHealthBool(false);
                            console.log(data);
                            console.log(ConfigFile);
                          })
                          .catch((err) => {
                            console.log(err);
                            setisDangerHealthBool(true);                            
                          });
                      }}
                    >
                      <h3> Health </h3>
                    </Button>
                  </FlexItem>
                  <FlexItem key={"celery_health"}>
                    <Button
                      variant="link"
                      isDanger={isDangerHealthCeleryBool}
                      onClick={() => {
                        fetch(`${url}/healthCelery`)
                          .then((response) => checkResponse(response))
                          .then((data) => {
                            console.log(data);
                            setisDangerHealthCeleryBool(false);
                          })
                          .catch((err) => {
                            console.log(err);
                            setisDangerHealthCeleryBool(true);
                          });
                      }}
                    >
                      <h3> Celery health </h3>
                    </Button>
                  </FlexItem>
                </Flex>
              </PanelMainBody>
            </PanelMain>
          </Panel>
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <Flex>
                  <OptoboardList
                    url={url}
                    DefaultConfig={DefaultConfig}
                    Configurations={Configurations}
                    ConfigFile={ConfigFile}
                  />
                </Flex>
              </PanelMainBody>
            </PanelMain>
          </Panel>
        </Page>

        <FooterComponentLogViewer Log={Log}/>

        <Modal
          variant={ModalVariant.large}
          bodyAriaLabel="Scrollable modal content"
          aria-label="modal for config input"
          tabIndex={0}
          isOpen={isOpenConfig}
          title={"Edit GUI Configuration"}
          onClose={() => {
            setisOpenConfig(false);
          }}
        >
          <Panel>
            <PanelMain>
              <PanelMainBody>
                <Panel>
                  <PanelMain>
                    <PanelMainBody>
                      <Flex>
                        <FlexItem>
                          <h2> Select default configuration </h2>
                        </FlexItem>
                        <FlexItem>
                          <FormSelect
                            value={newDefault}
                            onChange={(X) => {
                              setnewDefault(X);
                            }}
                            aria-label="FormSelect change default configuration"
                          >
                            {ConfigurationOptions.map((option, index) => (
                              <FormSelectOption
                                isDisabled={option.disabled}
                                key={index}
                                value={option.value}
                                label={option.label}
                              />
                            ))}
                          </FormSelect>
                        </FlexItem>
                        <FlexItem>
                          <Button
                            key="confirm1"
                            variant="primary"
                            onClick={() => {
                              if (newDefault !== "Choose Configuration") {
                                editConfig("defaultConfig", newDefault, url);
                              }
                            }}
                          >
                            Confirm
                          </Button>
                        </FlexItem>
                      </Flex>
                    </PanelMainBody>
                  </PanelMain>
                </Panel>

                <Panel>
                  <PanelMain>
                    <PanelMainBody>
                      <Flex>
                        <FlexItem>
                          <FormSelect
                            value={DefaultConfigChange}
                            onChange={(X) => {
                              setDefaultConfigChange(X);
                            }}
                            aria-label="FormSelect Input"
                          >
                            {ConfigurationOptions2Change.map(
                              (option, index) => (
                                <FormSelectOption
                                  isDisabled={option.disabled}
                                  key={index}
                                  value={option.value}
                                  label={option.label}
                                />
                              )
                            )}
                          </FormSelect>
                        </FlexItem>
                        <FlexItem>
                          <TextInput
                            value={newConfigName}
                            type="text"
                            aria-label="disabled text input example"
                            onChange={(value) => {
                              setnewConfigName(value);
                            }}
                            isDisabled={
                              DefaultConfigChange == "Add New Configuration"
                                ? false
                                : true
                            }
                          />
                        </FlexItem>
                        <FlexItem>
                          <Button
                            key="confirm2"
                            variant={optoConfigResult}
                            onClick={() => {
                              editConfig(
                                DefaultConfigChange == "Add New Configuration"
                                  ? newConfigName
                                  : DefaultConfigChange,
                                modalText, url
                              );
                            }}
                          >
                            Confirm
                          </Button>
                        </FlexItem>
                      </Flex>
                    </PanelMainBody>
                  </PanelMain>
                </Panel>
              </PanelMainBody>
            </PanelMain>
          </Panel>
          <CodeEditor
            code={modalText}
            isDarkTheme={false}
            isLineNumbersVisible={true}
            language={Language.javascript}
            onChange={(value) => setmodalText(value)}
            aria-label="text area vertical resize example"
            height="400px"
          />
        </Modal>
      </React.Fragment>
    );
  }
}
