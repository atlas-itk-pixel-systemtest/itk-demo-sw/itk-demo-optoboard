import React, { useState, useEffect } from "react";
import { WriteReadBox } from "../OptoboardTools/WriteReadBox";
import { Config } from "../OptoboardTools/Config";
import { OptoStatus } from "../OptoboardTools/OptoStatus";
import { EqualizationBox } from "../OptoboardTools/EqualizationBox";
import { PolarityBox } from "../OptoboardTools/PolarityBox";
import { PhaseBox } from "../OptoboardTools/PhaseBox";
import { DumpCustomConfig } from "../OptoboardTools/DumpCustomConfigBox";

import {
  Bullseye,
  Button,
  Divider,
  Flex,
  FlexItem,
  Text,
  TextContent,
  TextVariants,
  FormSelect,
  FormSelectOption,
  Tooltip,
  Panel,
  PanelMain,
  PanelMainBody,
  Icon,
} from "@patternfly/react-core";

import InfoCircleIcon from "@patternfly/react-icons/dist/esm/icons/info-circle-icon";

export function SingleOptoPanel({
  url,
  position,
  index,
  optoListInfo,
  optoList,
}) {
  const [device_val, setdevice_val] = useState("lpgbt1");
  const [isDisabledEqualizationButton, setisDisabledEqualizationButton] =
    useState(true);
  const [isDisabledPolarityButton, setisDisabledPolarityButton] =
    useState(true);
  const [isDisabledBertButton, setisDisabledBertButton] = useState(true);
  const [isDisabledPhaseButton, setisDisabledPhaseButton] = useState(true);
  const device = [
    {
      label: "lpgbt1",
      disabled: optoListInfo[position].components.lpgbt1 ? false : true,
      value: "lpgbt1",
    },
    {
      label: "lpgbt2",
      disabled: optoListInfo[position].components.lpgbt2 ? false : true,
      value: "lpgbt2",
    },
    {
      label: "lpgbt3",
      disabled: optoListInfo[position].components.lpgbt3 ? false : true,
      value: "lpgbt3",
    },
    {
      label: "lpgbt4",
      disabled: optoListInfo[position].components.lpgbt4 ? false : true,
      value: "lpgbt4",
    },
    {
      label: "gbcr1",
      disabled: optoListInfo[position].components.gbcr1 ? false : true,
      value: "gbcr1",
    },
    {
      label: "gbcr2",
      disabled: optoListInfo[position].components.gbcr2 ? false : true,
      value: "gbcr2",
    },
    {
      label: "gbcr3",
      disabled: optoListInfo[position].components.gbcr3 ? false : true,
      value: "gbcr3",
    },
    {
      label: "gbcr4",
      disabled: optoListInfo[position].components.gbcr4 ? false : true,
      value: "gbcr4",
    },
    { label: "vtrx", disabled: false, value: "vtrx" },
  ];

  useEffect(() => {
    if (device_val.includes("lpgbt")) {
      setisDisabledBertButton(false);
      setisDisabledEqualizationButton(true);
      setisDisabledPolarityButton(false);
      setisDisabledPhaseButton(false);
    } else if (device_val.includes("gbcr")) {
      setisDisabledBertButton(true);
      setisDisabledEqualizationButton(false);
      setisDisabledPolarityButton(true);
      setisDisabledPhaseButton(true);
    } else {
      setisDisabledBertButton(true);
      setisDisabledEqualizationButton(true);
      setisDisabledPolarityButton(true);
      setisDisabledPhaseButton(true);
    }
  }, [device_val]);

  return (
    <FlexItem key={index + "optoInList"}>
      <Panel variant="bordered">
        <PanelMain>
          <PanelMainBody>
            <Flex>
              <FlexItem flex={{ default: "flex_1" }}>
                <TextContent>
                  <Text component={TextVariants.h3}>{position}</Text>
                </TextContent>
                </FlexItem>
              <FlexItem align={{ default: "alignRight" }}>
                {/*                     <Tooltip content={"Display additional functions"}>
                <Button
                  aria-label="modify"
                  variant="plain"
                  id={"tt-ref" + index}
                  onClick={() => {
                    setisDrawerExpanded(!isDrawerExpanded);
                  }}
                >
                  <Icon>
                    <PlusCircleIcon />
                  </Icon>
                </Button>
              </Tooltip> */}
                <Tooltip
                  entryDelay={150}
                  exitDelay={200}
                  content={
                    <div>
                      <h4>
                        Serial Number: {optoList[position].optoboard_serial} /
                        Vtrx: {optoList[position].vtrx_v}{" "}
                      </h4>
                      <h4>
                        testmode: {optoList[position].test_mode.toString()} /
                        debug: {optoList[position].debug.toString()} / test:{" "}
                        {optoList[position].test_mode.toString()}
                      </h4>
                      <h4>
                        flx_G: {optoList[position].flx_G} / flx_d:{" "}
                        {optoList[position].flx_d} / commToolName:{" "}
                        {optoList[position].commToolName.toString()}{" "}
                      </h4>
                      
                      
                      
                    </div>
                  }
                >
                  <Button aria-label="drawer" variant="plain" id="tt-ref2">
                    <Icon>
                      <InfoCircleIcon />
                    </Icon>
                  </Button>
                </Tooltip>
              </FlexItem>
            </Flex>
            {/*                 <Drawer
            isExpanded={isDrawerExpanded}
            isInline={false}
            position="right"
          >
            <DrawerContent
              panelContent={
                <DrawerPanelContent>
                  <DrawerHead>ascas</DrawerHead>
                </DrawerPanelContent>
              }
            >
              <DrawerContentBody></DrawerContentBody>
            </DrawerContent>
          </Drawer> */}
          <Flex>
          <FlexItem>
            <FlexItem>
                <Bullseye>
                  <h4> Select device: </h4>
                </Bullseye>
                <Bullseye>
                  <FormSelect
                    id={"selectDeviceSimplePanelRead"}
                    value={device_val}
                    items={device}
                    onChange={(x) => setdevice_val(x)}
                  >
                    {device.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
                </FlexItem>
                </FlexItem>
              <FlexItem>
                <WriteReadBox
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                  vtrxVersion={optoListInfo[position].vtrx_v}
                  device_val={device_val}
                  isDisabledBertButton={isDisabledBertButton}
                />
              </FlexItem>
              <FlexItem>
                <EqualizationBox
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                  device_val={device_val}
                  isDisabledEqualizationButton={isDisabledEqualizationButton}
                />
                <PolarityBox
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                  device_val={device_val}
                  isDisabledPolarityButton={isDisabledPolarityButton}
                />
                <PhaseBox
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                  device_val={device_val}
                  isDisabledPhaseButton={isDisabledPhaseButton}
                />
              </FlexItem>
              <Divider
                orientation={{
                  default: "vertical",
                }}
              />
              <FlexItem>
                <Config
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                />
                <OptoStatus
                  url={url}
                  optoboardPosition={position}
                  optoComponents={optoListInfo[position].components}
                />
                <DumpCustomConfig
                  url={url}
                  optoboardPosition={position}
                />
              </FlexItem>
            </Flex>
          </PanelMainBody>
        </PanelMain>
      </Panel>
    </FlexItem>
  );
}
