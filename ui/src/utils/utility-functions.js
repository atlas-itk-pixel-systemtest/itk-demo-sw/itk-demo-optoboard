
export function getPostBody(data) {
  return ({
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow', 
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  });
}

export function checkResponse(response) {
  switch (response.status) {
    case 200:
      return response.json();
    case 400:
      throw new Error(`The provided arguments are not valid. Your request "${response.url}" could not be completed.`);
    case 404:
      // return "URL DOESN'T EXIST"
      throw new Error(`The requested url "${response.url}" does not exist.`);
    case 500:
      throw new Error(`An internal server error occured. Your request "${response.url}" could not be completed.`);
      //return "WRONG INPUT" 
    case 503:
      throw new Error(`Service unavailable! Verify the software and hardware connection to the Optoboard!`)
    default:
      //return "UNKNOWN ERROR"
      throw new Error(`An unknown Error occured in your request "${response.url}".`);
  }
}
