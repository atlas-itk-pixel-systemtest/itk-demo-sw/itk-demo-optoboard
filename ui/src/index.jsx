import React from 'react'
import ReactDOM from 'react-dom/client'
import "@patternfly/react-core/dist/styles/base.css";
import { OptoboardGUI } from "./UI_architecture/OptoboardGUI";

// the url provided in the ReactDOM is used inside the gui for the fetch commands
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <>
    <OptoboardGUI
      key="main-content"
    />
  </>
)
// /optoboard/api
// http://localhost:5009/optoboard/api
// <SimpleTabs backendUrl="http://localhost:5009" />
