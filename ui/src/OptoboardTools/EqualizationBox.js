import React, { useState } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Flex,
  FlexItem,
  Bullseye,
  FormSelect,
  FormSelectOption,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from "../utils/utility-functions";

export function EqualizationBox({
  url,
  optoboardPosition,
  optoComponents,
  device_val,
  isDisabledEqualizationButton,
}) {
  const [channel, setchannel] = useState(0);
  const [isSettingEqualization, setisSettingEqualization] = useState(false);
  const [buttonVariantGBCR, setbuttonVariantGBCR] = useState("primary");
  const [mFreq, setMFreq] = useState(0);
  const [hFreq, setHFreq] = useState(0);

  const channels = [...Array(7).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const mFreqs = [...Array(16).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const hFreqs = [...Array(16).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const gbcrEqualization = (url) => {
    const requestData = {
      optoboardPosition: optoboardPosition,
      device: device_val,
      channel: channel.toString(),
      mFreq: mFreq.toString(),
      hFreq: hFreq.toString(),
    };
    setisSettingEqualization(true);
    setbuttonVariantGBCR("primary");
    fetch(`${url}/setGBCREqualization`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        setbuttonVariantGBCR("danger");
        console.log(err);
      })
      .finally(setisSettingEqualization(false));
  };

  return (
    <React.Fragment>
      <Panel>
        <PanelMain>
          <PanelMainBody>
            <Flex>
              <FlexItem>
                <Button
                  isDisabled={isDisabledEqualizationButton}
                  spinnerAriaValueText={
                    isSettingEqualization ? "Loading" : undefined
                  }
                  isLoading={isSettingEqualization}
                  id="gbcrEqualization"
                  variant={buttonVariantGBCR}
                  onClick={(event) => {
                    console.log("DEVICE: ", device_val);
                    gbcrEqualization(url);
                  }}
                >
                  {isSettingEqualization
                    ? "Setting equalization..."
                    : "Set Equalization"}
                </Button>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <h4>Channel</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    id={"selectChannelEqualization"}
                    value={channel}
                    items={channels}
                    onChange={(x) => setchannel(x)}
                  >
                    {channels.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <h4>Med Frequency</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <FormSelect
                    id={"selectMFreq"}
                    value={mFreq}
                    items={mFreqs}
                    onChange={(x) => setMFreq(x)}
                  >
                    {mFreqs.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <h4>High Frequency</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <FormSelect
                    id={"selectHFreq"}
                    value={hFreq}
                    items={hFreqs}
                    onChange={(x) => setHFreq(x)}
                  >
                    {mFreqs.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
          </PanelMainBody>
        </PanelMain>
      </Panel>
    </React.Fragment>
  );
}
