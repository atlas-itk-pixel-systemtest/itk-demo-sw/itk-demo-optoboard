import React, { useState } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Bullseye,
  Flex,
  FlexItem,
  Icon,
} from "@patternfly/react-core";
import { getPostBody, checkResponse } from "../utils/utility-functions";
import ExclamationCircleIcon from "@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon";
import CheckCircleIcon from "@patternfly/react-icons/dist/esm/icons/check-circle-icon";
import BanIcon from "@patternfly/react-icons/dist/esm/icons/ban-icon";

export function OptoStatus({ url, optoboardPosition, optoComponents }) {
  const [isLoading, setisLoading] = useState(false);
  const [buttonVariantStatus, setButtonVariantStatus] = useState("primary");
  const [Status1, setStatus1] = useState(
    <Icon status={optoComponents.lpgbt1 ? "info" : ""}>
      <BanIcon />
    </Icon>
  );
  const [Status2, setStatus2] = useState(
    <Icon status={optoComponents.lpgbt2 ? "info" : ""}>
      <BanIcon />
    </Icon>
  );
  const [Status3, setStatus3] = useState(
    <Icon status={optoComponents.lpgbt3 ? "info" : ""}>
      <BanIcon />
    </Icon>
  );
  const [Status4, setStatus4] = useState(
    <Icon status={optoComponents.lpgbt4 ? "info" : ""}>
      <BanIcon />
    </Icon>
  );

  const StatusCheck = (url) => {
    const requestData = {
      optoboardPosition: optoboardPosition,
    };
    setisLoading(true);
    fetch(`${url}/optoStatus`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        setButtonVariantStatus("primary");
        setStatus1(
          data.lpgbt1 === 1 ? (
            <Icon status="success">
              <CheckCircleIcon />
            </Icon>
          ) : (
            <Icon status="danger">
              <ExclamationCircleIcon />
            </Icon>
          )
        );
        setStatus2(
          data.lpgbt2 === 1 ? (
            <Icon status="success">
              <CheckCircleIcon />
            </Icon>
          ) : (
            <Icon status="danger">
              <ExclamationCircleIcon />
            </Icon>
          )
        );
        setStatus3(
          data.lpgbt3 === 1 ? (
            <Icon status="success">
              <CheckCircleIcon />
            </Icon>
          ) : (
            <Icon status="danger">
              <ExclamationCircleIcon />
            </Icon>
          )
        );
        setStatus4(
          data.lpgbt4 === 1 ? (
            <Icon status="success">
              <CheckCircleIcon />
            </Icon>
          ) : (
            <Icon status="danger">
              <ExclamationCircleIcon />
            </Icon>
          )
        );
      })
      .catch((err) => {
        console.log(err);
        setButtonVariantStatus("danger");
      })
      .finally(() => {
        setisLoading(false);
      })
  };

  return (
    <React.Fragment>
      <Flex direction={{ default: "column" }}>
        <Panel variant="">
          <PanelMain>
            <PanelMainBody>
              <Bullseye>
                <Button
                  spinnerAriaValueText={isLoading ? "Loading" : undefined}
                  isLoading={isLoading}
                  id="readstatus"
                  variant={buttonVariantStatus}
                  onClick={(event) => StatusCheck(url)}
                >
                  {isLoading ? "Reading..." : "Read Status"}
                </Button>
              </Bullseye>

              <Bullseye>
                <Flex>
                  <FlexItem>
                    <h3>lpGBT status</h3>
                  </FlexItem>
                  <FlexItem>
                    <Bullseye>
                      {Status1}
                      {Status2}
                      {Status3}
                      {Status4}
                    </Bullseye>
                  </FlexItem>
                </Flex>
              </Bullseye>
            </PanelMainBody>
          </PanelMain>
        </Panel>
      </Flex>
    </React.Fragment>
  );
}
