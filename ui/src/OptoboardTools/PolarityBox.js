import React, { useState, useEffect } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Flex,
  FlexItem,
  Bullseye,
  FormSelect,
  FormSelectOption,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from "../utils/utility-functions";

export function PolarityBox({
  url,
  optoboardPosition,
  optoComponents,
  device_val,
  isDisabledPolarityButton,
}) {
  const [buttonVariantPolarity, setbuttonVariantPolarity] = useState("primary");
  const [isSwappingPolarization, setisSwappingPolarization] = useState(false);
  const [linkNumber, setlinkNumber] = useState(0);
  const [TxRx, setTxRx] = useState("Tx");

  const TxRxs = ["Tx", "Rx"].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const [linkNumbers, setLinkNumbers] = useState(
    [...Array(8).keys()].map((x) => {
      return { label: x, disabled: false, value: x };
    })
  );

  useEffect(() => {
    if (TxRx == "Tx") {
      setLinkNumbers(
        [...Array(8).keys()].map((x) => {
          return { label: x, disabled: false, value: x };
        })
      );
    } else if (TxRx == "Rx") {
      setLinkNumbers(
        [...Array(6).keys()].map((x) => {
          return { label: x, disabled: false, value: x };
        })
      );
    }
  }, [TxRx]);

  const swapPolarity = (url) => {
    const requestData = {
      optoboardPosition: optoboardPosition,
      device: device_val,
      TxRx: TxRx,
      link: linkNumber.toString(),
    };
    console.log(requestData);
    setisSwappingPolarization(true);
    setbuttonVariantPolarity("primary");
    fetch(`${url}/swapPolarity`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        setbuttonVariantPolarity("danger");
        console.log(err);
      })
      .finally(setisSwappingPolarization(false));
  };

  return (
    <React.Fragment>
      <Panel>
        <PanelMain>
          <PanelMainBody>
            <Flex>
              <FlexItem>
                <Bullseye>
                  <Button
                    isDisabled={isDisabledPolarityButton}
                    spinnerAriaValueText={
                      isSwappingPolarization ? "Loading" : undefined
                    }
                    isLoading={isSwappingPolarization}
                    id="swappingPolarization"
                    variant={buttonVariantPolarity}
                    onClick={(event) => {
                      console.log("DEVICE: ", device_val);
                      swapPolarity(url);
                    }}
                  >
                    {isSwappingPolarization
                      ? "Swapping polarity"
                      : "Swap polarity"}
                  </Button>
                </Bullseye>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <h4>Link (Rx/Tx)</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem align={{ default: "alignRight" }}>
                <FormSelect
                  id={"selectLinkType"}
                  value={TxRx}
                  items={TxRxs}
                  onChange={(x) => setTxRx(x)}
                >
                  {TxRxs.map((option, index) => (
                    <FormSelectOption
                      isDisabled={option.disabled}
                      key={index}
                      value={option.value}
                      label={option.label}
                    />
                  ))}
                </FormSelect>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <h4>Link number</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem align={{ default: "alignRight" }}>
                <Bullseye>
                  <FormSelect
                    id={"selectLinkNumber"}
                    value={linkNumber}
                    items={linkNumbers}
                    onChange={(x) => setlinkNumber(x)}
                  >
                    {linkNumbers.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
          </PanelMainBody>
        </PanelMain>
      </Panel>
    </React.Fragment>
  );
}