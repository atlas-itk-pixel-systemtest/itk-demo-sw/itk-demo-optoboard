import React, { useState, useEffect } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Flex,
  FlexItem,
  Bullseye,
  Tooltip,
  Select,
  SelectOption,
  SelectVariant,
  FormSelect,
  FormSelectOption,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from "../utils/utility-functions";
import { Registers } from "../Config/Registers";

export function WriteReadBox({
  url,
  optoboardPosition,
  optoComponents,
  vtrxVersion,
  device_val,
  isDisabledBertButton,
}) {
  const [isBERTing, setisBERTing] = useState(false);
  const [channel, setchannel] = useState(0);
  // goes up to 6
  const [meastime, setmeastime] = useState(12);
  const [buttonVariantBERT, setbuttonVariantBERT] = useState("primary");
  const [berLimit, setberLimit] = useState("");

  const channels = [...Array(7).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const meastimes = [...Array(16).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const berTest = (url) => {
    //var re = /[0-9A-Fa-f]{6}/g;
    const requestData = {
      optoboardPosition: optoboardPosition,
      meastime: meastime.toString(),
      channel: channel.toString(),
      device: device_val,
    };
    setisBERTing(true);
    setberLimit("");
    setbuttonVariantBERT("primary");
    fetch(`${url}/BERT`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        setberLimit(data.BER_limit);
      })
      .catch((err) => {
        setbuttonVariantBERT("danger");
        console.log(err);
      })
      .finally(setisBERTing(false));
  };

  // functions to read a register from the Optoboard

  const [reg_read, setreg_read] = useState("");
  const [read_back_read, setread_back_read] = useState("");
  const [isOpenRead, setisOpenRead] = useState(false);

  const onSelectRead = (event, select, isPlaceholder) => {
    if (isPlaceholder) {
      clearSelectionRead();
    } else {
      setreg_read(select);
      setisOpenRead(false);
      console.log("selected:", select);
    }
  };

  const clearSelectionRead = () => {
    console.log("cancel input/selection");
    setreg_read("");
    setisOpenRead(false);
  };

  // function to implement the filter of the registers
  const [optionsRead, setoptionsRead] = useState([]);

  const customFilterRead = (_, value) => {
    if (!value) {
      return optionsRead;
    }
    const input = new RegExp(value, "i");
    return optionsRead.filter((child) => input.test(child.props.value));
  };

  const read_device = (reg_address, url) => {
    //var re = /[0-9A-Fa-f]{6}/g;
    const requestData = {
      optoboardPosition: optoboardPosition,
      register: reg_address,
      device: device_val,
    };
    fetch(`${url}/readRegister`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        setbuttonVariantRead("primary");
        setread_back_read(data);
      })
      .catch((err) => {
        setbuttonVariantRead("danger");
        console.log(err);
      });
  };

  // functions to write a register on the Optoboard

  const [reg_write, setreg_write] = useState("");
  const [value_write, setvalue_write] = useState("");
  const [read_back_write, setread_back_write] = useState("");
  const [isOpenWrite, setisOpenWrite] = useState(false);
  const [isOpenValue, setisOpenValue] = useState(false);

  const [buttonVariantRead, setbuttonVariantRead] = useState("primary");
  const [buttonVariantWrite, setbuttonVariantWrite] = useState("primary");

  const onSelectWrite = (event, select, isPlaceholder) => {
    if (isPlaceholder) {
      clearSelectionWrite();
    } else {
      setreg_write(select);
      setisOpenWrite(false);
      console.log("selected:", select);
      let x = [];
      let selectArray = select.split(" ");
      // console.log("selected:", select.split(" ").length);

      let keyRegValue = "";

      if (device_val.includes("lpgbt")) {
        keyRegValue =
          "lpgbt" + (optoComponents.lpgbt_v ? "v1" : "v0") + "Write";
      } else if (device_val.includes("gbcr")) {
        keyRegValue = "gbcr" + (optoComponents.gbcr_v == 2 ? "v2" : "v3");
      } else if (device_val.includes("vtrx")) {
        keyRegValue = "vtrx" + (vtrxVersion == "1.2" ? "v12" : "v13");
      }

      if (selectArray.length == 1) {
        for (
          var i = Registers[keyRegValue][select]["values"][0];
          i <= Registers[keyRegValue][select]["values"][1];
          i++
        ) {
          x.push(<SelectOption key={i.toString()} value={i.toString()} />);
        }
      } else if (selectArray.length == 2) {
        let subRegObj = Registers[keyRegValue][selectArray[0]][
          "subregisters"
        ].filter((sreg) => Object.keys(sreg) == selectArray[1]);
        if (subRegObj.length == 1) {
          subRegObj = subRegObj[0];
          for (
            var i = subRegObj[selectArray[1]][0];
            i <= subRegObj[selectArray[1]][1];
            i++
          ) {
            x.push(<SelectOption key={i.toString()} value={i.toString()} />);
          }
        }
      }
      setoptionsValue(x);
    }
  };

  const clearSelectionWrite = () => {
    console.log("cancel input/selection");
    setreg_write("");
    setisOpenWrite(false);
    setoptionsValue([]);
  };

  // function to implement the filter of the registers

  const [optionsWrite, setoptionsWrite] = useState([]);

  const customFilterWrite = (_, value) => {
    if (!value) {
      return optionsWrite;
    }
    const input = new RegExp(value, "i");
    return optionsWrite.filter((child) => input.test(child.props.value));
  };

  const onSelectValue = (event, select, isPlaceholder) => {
    if (isPlaceholder) {
      clearSelectionValue();
    } else {
      setvalue_write(select);
      setisOpenValue(false);
      console.log("selected:", select);
    }
  };

  const clearSelectionValue = () => {
    console.log("cancel input/selection");
    setvalue_write("");
    setisOpenValue(false);
  };

  // function to implement the filter of the values to write

  const [optionsValue, setoptionsValue] = useState([]);

  const customFilterValue = (_, value) => {
    if (!value) {
      return optionsValue;
    }
    const input = new RegExp(value, "i");
    return optionsValue.filter((child) => input.test(child.props.value));
  };

  const write_device = (reg_address, new_reg_value, url) => {
    const requestData = {
      optoboardPosition: optoboardPosition,
      device: device_val,
      register: reg_address,
      newValue: new_reg_value,
    };
    fetch(`${url}/writeRegister`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        setbuttonVariantWrite("primary");
        setread_back_write(data);
      })
      .catch((err) => {
        setbuttonVariantWrite("danger");
        console.log(err);
      });
  };

  useEffect(() => {
    const optionsChange = () => {
      setoptionsRead([]);
      setoptionsWrite([]);
      let keyRegRead = "",
        keyRegWrite = "";

      if (device_val.includes("lpgbt")) {
        keyRegRead = "lpgbt" + (optoComponents.lpgbt_v ? "v1" : "v0") + "Read";
        keyRegWrite =
          "lpgbt" + (optoComponents.lpgbt_v ? "v1" : "v0") + "Write";
      } else if (device_val.includes("gbcr")) {
        keyRegRead = "gbcr" + (optoComponents.gbcr_v == 2 ? "v2" : "v3");
        keyRegWrite = "gbcr" + (optoComponents.gbcr_v == 2 ? "v2" : "v3");
      } else if (device_val.includes("vtrx")) {
        keyRegRead = "vtrx" + (vtrxVersion == "1.2" ? "v12" : "v13");
        keyRegWrite = "vtrx" + (vtrxVersion == "1.2" ? "v12" : "v13");
      }

      let x = [];
      Object.keys(Registers[keyRegRead]).map((register, index) => {
        x.push(<SelectOption key={register} value={register} />);
        Registers[keyRegRead][register]["subregisters"].map((subreg) => {
          x.push(
            <SelectOption
              key={register + " " + Object.keys(subreg)[0]}
              value={register + " " + Object.keys(subreg)[0]}
            />
          );
        });
      });

      let y = [];
      Object.keys(Registers[keyRegWrite]).map((register, index) => {
        y.push(<SelectOption key={register} value={register} />);
        Registers[keyRegWrite][register]["subregisters"].map((subreg) => {
          y.push(
            <SelectOption
              key={register + " " + Object.keys(subreg)[0]}
              value={register + " " + Object.keys(subreg)[0]}
            />
          );
        });
      });

      setoptionsRead(x);
      setoptionsWrite(y);
    };

    optionsChange();
  }, [device_val]);

  // Return

  return (
    <React.Fragment>
      <Panel>
        <PanelMain>
          <PanelMainBody>
            <Bullseye>
              <Flex>
                <Bullseye>
                  <FlexItem>
                    <Bullseye>
                      <Tooltip
                        content={
                          <div>
                            Use rom register for testing. In lpgbt v1 the return
                            has to be 166.{" "}
                          </div>
                        }
                      >
                        <Button
                          id="read_reg_send_command"
                          variant={buttonVariantRead}
                          onClick={() => read_device(reg_read, url)}
                        >
                          <h3> Read register </h3>
                        </Button>
                      </Tooltip>
                    </Bullseye>
                  </FlexItem>
                </Bullseye>

                <FlexItem>
                  <Bullseye>
                    <h4>Read back: {read_back_read}</h4>
                  </Bullseye>
                </FlexItem>
              </Flex>
            </Bullseye>
            <Bullseye>
              <Flex>
                <FlexItem>
                  <Select
                    maxHeight={300}
                    variant={SelectVariant.typeahead}
                    direction={"down"}
                    typeAheadAriaLabel="Select a register"
                    onToggle={() => setisOpenRead(!isOpenRead)}
                    onTypeaheadInputChanged={(value) => setreg_read(value)}
                    onSelect={onSelectRead}
                    onClear={clearSelectionRead}
                    onFilter={customFilterRead}
                    selections={reg_read}
                    isOpen={isOpenRead}
                    aria-labelledby={"typeahead-select-id-2"}
                    placeholderText="Select a register"
                    noResultsFoundText={"No results found"}
                  >
                    {optionsRead}
                  </Select>
                </FlexItem>
              </Flex>
            </Bullseye>
          </PanelMainBody>
        </PanelMain>
      </Panel>

      <Panel>
        <PanelMain>
          <Bullseye>
            <Flex>
              <Bullseye>
                <FlexItem>
                  <Bullseye>
                    <Button
                      id="write_pri_send_command"
                      variant={buttonVariantWrite}
                      onClick={() => write_device(reg_write, value_write, url)}
                    >
                      <h3> Write register </h3>
                    </Button>
                  </Bullseye>
                </FlexItem>
              </Bullseye>

              <FlexItem>
                <Bullseye>
                  <h4>Read back: {read_back_write}</h4>
                </Bullseye>
              </FlexItem>
            </Flex>
          </Bullseye>
          <Bullseye>
            <Flex>
              <FlexItem>
                <Bullseye>
                  <Select
                    maxHeight={300}
                    variant={SelectVariant.typeahead}
                    direction={"down"}
                    typeAheadAriaLabel="Select a register write"
                    onToggle={() => setisOpenWrite(!isOpenWrite)}
                    onTypeaheadInputChanged={(value) => {
                      setreg_write(value);
                      setoptionsValue([]);
                    }}
                    onSelect={onSelectWrite}
                    onClear={clearSelectionWrite}
                    onFilter={customFilterWrite}
                    selections={reg_write}
                    isOpen={isOpenWrite}
                    aria-labelledby={"typeahead-select-id-2-write"}
                    placeholderText="Select a register"
                    noResultsFoundText={"No results found"}
                  >
                    {optionsWrite}
                  </Select>
                </Bullseye>
              </FlexItem>
            </Flex>
          </Bullseye>
          <Bullseye>
            <Flex>
              {/* <FlexItem>
                <Bullseye>
                  <h4>Value</h4>
                </Bullseye>
              </FlexItem> */}

              <FlexItem>
                <Bullseye>
                  <Select
                    maxHeight={300}
                    variant={SelectVariant.typeahead}
                    direction={"down"}
                    typeAheadAriaLabel="Value to be written"
                    onToggle={() => setisOpenValue(!isOpenValue)}
                    onTypeaheadInputChanged={(value) => setvalue_write(value)}
                    onSelect={onSelectValue}
                    onClear={clearSelectionValue}
                    onFilter={customFilterValue}
                    selections={value_write}
                    isOpen={isOpenValue}
                    aria-labelledby={"typeahead-select-id-2-valuetowrite"}
                    placeholderText="Value to be written"
                    noResultsFoundText={"Range not available for this register"}
                  >
                    {optionsValue}
                  </Select>
                  {/* <TextInput
                    id="write_value"
                    name="write_value"
                    value={value_write}
                    onChange={(x) => setvalue_write(x)}
                  /> */}
                </Bullseye>
              </FlexItem>
            </Flex>
          </Bullseye>
        </PanelMain>
      </Panel>

      <Panel>
        <PanelMain>
          <PanelMainBody>
            <Flex>
              <FlexItem align={{ default: 'alignCenter' }}>
                <Button
                  isDisabled={isDisabledBertButton}
                  spinnerAriaValueText={isBERTing ? "Loading" : undefined}
                  isLoading={isBERTing}
                  id="berTest"
                  variant={buttonVariantBERT}
                  onClick={(event) => {
                    console.log(device_val);
                    berTest(url);
                  }}
                >
                  {isBERTing ? "Doing BERT..." : "Perform BERT"}
                </Button>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <h4>BER limit: {berLimit}</h4>
                </Bullseye>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                <Bullseye>
                  <h4>Uplink</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    id={"selectChannelBert"}
                    value={channel}
                    items={channels}
                    onChange={(x) => setchannel(x)}
                  >
                    {channels.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <h4>Meas Time</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    id={"selectChannelBert"}
                    value={meastime}
                    items={meastimes}
                    onChange={(x) => setmeastime(x)}
                  >
                    {meastimes.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
          </PanelMainBody>
        </PanelMain>
      </Panel>
    </React.Fragment>
  );
}
