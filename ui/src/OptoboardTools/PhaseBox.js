import React, { useState, useEffect } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Flex,
  FlexItem,
  Bullseye,
  FormSelect,
  FormSelectOption,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from "../utils/utility-functions";

export function PhaseBox({
  url,
  optoboardPosition,
  device_val,
  isDisabledPhaseButton,
}) {
  const [phaseMode, setPhaseMode] = useState("");
  const [isSettingPhaseMode, setisSettingPhaseMode] = useState(false);
  const [buttonVariantLpgbt, setbuttonVariantLpgbt] = useState("primary");
  const [group, setgroup] = useState(0);
  const [phase, setphase] = useState(0);
  const [isNotFixedMode, setisNotFixedMode] = useState(false);

  const groups = [...Array(6).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const phases = [...Array(16).keys()].map((x) => {
    return { label: x, disabled: false, value: x };
  });

  const phaseModes = [
    {
      label: "Select mode",
      disabled: false,
      value: "",
      isPlaceholder: true,
    },
    {
      label: "Fixed",
      disabled: false,
      value: "fixed_phase",
    },
    {
      label: "Continuous",
      disabled: false,
      value: "continuous_tracking",
    },
    {
      label: "Training",
      disabled: false,
      value: "initial_training",
    },
  ];

  useEffect(() => {
    if (phaseMode == "fixed_phase") {
      setisNotFixedMode(false);
    } else {
      setisNotFixedMode(true);
    }
  }, [phaseMode]);

  const setLpgbtPhase = (url) => {
    const requestData = {
      optoboardPosition: optoboardPosition,
      device: device_val,
      phaseMode: phaseMode,
      group: group.toString(),
      phase: phase.toString(),
    };
    setisSettingPhaseMode(true);
    setbuttonVariantLpgbt("primary");
    fetch(`${url}/setLpgbtPhase`, getPostBody(requestData))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        setbuttonVariantLpgbt("danger");
        console.log(err);
      })
      .finally(setisSettingPhaseMode(false));
  };

  return (
    <React.Fragment>
      <Panel>
        <PanelMain>
          <PanelMainBody>
            <Flex>
              <FlexItem align={{ default: "alignCenter" }}>
                <Button
                  isDisabled={isDisabledPhaseButton}
                  spinnerAriaValueText={
                    isSettingPhaseMode ? "Loading" : undefined}
                  isLoading={isSettingPhaseMode}
                  id="lpgbtPhase"
                  variant={buttonVariantLpgbt}
                  onClick={(event) => {
                    setLpgbtPhase(url);
                  }}
                >
                  {isSettingPhaseMode ? "Setting phase mode..." : "Set Phase Mode"}
                </Button>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    id={"selectPhaseMode"}
                    value={phaseMode}
                    items={phaseModes}
                    onChange={(x) => setPhaseMode(x)}
                  >
                    {phaseModes.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        isPlaceholder={option.isPlaceholder}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
            <Flex>
              <FlexItem>
                <Bullseye>
                  <h4>Group</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    id={"selectGroup"}
                    value={group}
                    items={groups}
                    onChange={(x) => setgroup(x)}
                  >
                    {groups.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <h4>Phase</h4>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Bullseye>
                  <FormSelect
                    isDisabled={isNotFixedMode}
                    id={"selectPhase"}
                    value={phase}
                    items={phases}
                    onChange={(x) => setphase(x)}
                  >
                    {phases.map((option, index) => (
                      <FormSelectOption
                        isDisabled={option.disabled}
                        key={index}
                        value={option.value}
                        label={option.label}
                      />
                    ))}
                  </FormSelect>
                </Bullseye>
              </FlexItem>
            </Flex>
          </PanelMainBody>
        </PanelMain>
      </Panel>
    </React.Fragment>
  );
}
