import React from 'react';
import { LogViewer } from '@patternfly/react-log-viewer';
import { Button } from '@patternfly/react-core';

export function FooterComponentLogViewer ({Log}) {
    const logViewerRef = React.useRef();
    const FooterButton = () => {
      const handleClick = () => {
          console.log(logViewerRef.current); 
        logViewerRef.current.scrollToBottom();
      };
      return <Button onClick={handleClick}>Jump to the bottom</Button>;
    };  
    return ( <LogViewer
    ref={logViewerRef}
    hasLineNumbers={false}
    height={300}
    data={Log}
    theme="light"
    footer={<FooterButton />}
    scrollToRow={(Log.match(/\n/g) || []).length}
    /> )
  };
