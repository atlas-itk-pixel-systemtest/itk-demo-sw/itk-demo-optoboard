import React from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Grid,
  GridItem,
  Bullseye,
} from "@patternfly/react-core";
import { getPostBody, checkResponse } from '../utils/utility-functions'


class CharacteristicHelperFunc extends React.Component {
	constructor(props) {
		super(props);
		};
render (){
	return (
	<React.Fragment>
		<GridItem span={3}>
		<Bullseye>
			<h4>{this.props.label}</h4>
		</Bullseye>
		</GridItem>
		<GridItem span={3}>
		<Bullseye>
			<h4>{this.props.value}</h4>		
		</Bullseye>
		</GridItem>
	</React.Fragment>
)};}


class Characteristics_lpGBT extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			params : [],
			charList : [
				{value: "", label: "VREF"}, 
				{value: "", label: "VDDIO"}, 
				{value: "", label: "VDDTX"}, 
				{value: "", label: "VDDRX"}, 
				{value: "", label: "VDD"}, 
				{value: "", label: "Temp"}
			],
			isLoading: false,
		};

	this.onError = (err) => {
		console.log("There was an Error:");
		console.log(err.message);
	} 

	this.get_characteristics = () => {
		const state = this.state;
		this.setState({
			isLoading: true
		});

		const body = {
			device: this.props.device,
		}
		console.log(body);
		fetch(`${this.props.backendUrl}/testCharacteristics_wrapper`, getPostBody(body)).then(response => checkResponse(response)).then(
	    data => {
				this.setState({isLoading: false});
	      if (data.errors.length === 0) {
	      	this.setState({
		      		...state,
		      		charList: [
								{value: data.data.VREF, label: "VREF"}, 
								{value: data.data.VDDIO, label: "VDDIO"}, 
								{value: data.data.VDDTX, label: "VDDTX"}, 
								{value: data.data.VDDRX, label: "VDDRX"}, 
								{value: data.data.VDD, label: "VDD"}, 
								{value: data.data.Temp, label: "Temp"}
		      		]
		      	});
	      }
	    }
	  ).catch(err => {this.setState({isLoading: false}); console.log(err)})}

	};


	render (){
		return (
	<React.Fragment>

		<Panel variant="bordered">

			<PanelMain>
				<PanelMainBody>

					<Grid>

						<GridItem span={12}>
							<Bullseye>
								<Button 
									spinnerAriaValueText={this.state.isLoading ? 'Loading' : undefined}
									isLoading={this.state.isLoading}
									id="readCharacteristics"
									variant="primary"
									onClick={(event) => {
										console.log(`Backend Url: ${this.props.backendUrl}`);
										this.get_characteristics();
									}}
								>
									Characteristics lpGBT{this.props.lpGBTNum}
								</Button>
							</Bullseye>
						</GridItem>

					{this.state.charList.map((x) => (
					<CharacteristicHelperFunc label={x.label} value={x.value}/>
					))}

					</Grid>
				</PanelMainBody>
			</PanelMain>
		</Panel>
	</React.Fragment>
);};	
}

export {Characteristics_lpGBT};
