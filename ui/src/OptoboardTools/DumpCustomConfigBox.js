import React, { useState, useEffect } from "react";
import {
    Button,
    PanelMainBody,
    PanelMain,
    Panel,
    Bullseye,
    Flex,
    FlexItem,
    TextInput,
} from "@patternfly/react-core";
import {getPostBody, checkResponse} from "../utils/utility-functions";

function DumpCustomConfig(
    {
        url,
        optoboardPosition,
    }
) {
    const [isLoading, setisLoading] = useState(false);
    const [buttonVariant, setbuttonVariant] = useState("primary");
    const [filename, setfilename] = useState("custom_config");

    return (

        <React.Fragment>
            <Panel variant="">
                <PanelMain>
                    <Bullseye>                       
                    <Button
                      spinnerAriaValueText={
                        isLoading ? "Creatingfile" : undefined
                      }
                      isLoading={isLoading}
                      variant={buttonVariant}
                      onClick={() => {
                        setisLoading(true);
                        console.log("Creating file called: " + filename.toString())
                        fetch(`${url}/dumpCustomRegConfig`, getPostBody({optoboardPosition:optoboardPosition,filename:filename.toString()}))
                          .then((response) => checkResponse(response))
                          .then((data) => {
                            console.log(data);
                            setbuttonVariant("primary");
                          })
                          .catch((err) => {
                            setbuttonVariant("danger");
                            console.log(err);
                          })
                          .finally(() => {
                            setisLoading(false);
                          });
                      }}
                    > 
                    {isLoading ? "Creating file... " : "Create custom file"}                        
                </Button>
                </Bullseye>
                <Bullseye>
                    <h4> Filename: </h4>
                    <TextInput
                    value={filename.value}
                    placeholder="Default: custom_config"
                    type="text"
                    id="textinput"
                    onChange={(value) => {
                        setfilename(value)
                    }}
                    >
                    </TextInput>
                </Bullseye>
            </PanelMain>
        </Panel>
    </React.Fragment>


    )
}

export { DumpCustomConfig };