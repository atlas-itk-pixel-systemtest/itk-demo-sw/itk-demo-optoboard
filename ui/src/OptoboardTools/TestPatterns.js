import React from "react";
import {
  Button,
  Text,
  TextInput,
  TextContent,
  TextVariants,
  PanelMainBody,
  PanelMain,
  Panel,
  Flex, 
  FlexItem,
  Grid,
  GridItem,
  Bullseye,
  Tooltip, 
  PanelHeader, 
  Modal, 
  ModalVariant,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from '../utils/utility-functions'
import { FormSelectInputNewer } from "../Widgets/FormSelectInput";
//import { OutlinedQuestionCircleIcon } from '@patternfly/react-icons/dist/esm/icons/outlined-question-circle-icon'

class HelperUplinkTestPatterns extends React.Component {
	constructor(props) {
		super(props);
	};

	render (){
	return (
	<React.Fragment>

	<Flex spaceItems={{ default: 'spaceItemsNone' }}>
  	<FlexItem flex={{ default: 'flex_1' }}>
			<Bullseye>
				<h4> {this.props.name} </h4>
			</Bullseye>
		</FlexItem>

		<FlexItem flex={{ default: 'flex_1' }}>
			<Bullseye>
				<FormSelectInputNewer id={this.props.name} items={this.props.choices} value={this.props.value} onChange={this.props.onChange}/>
			</Bullseye>
		</FlexItem>

	</Flex>

	</React.Fragment>
);};
};




class TestPatterns extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			DataSource : [{ label: 'EPORTRX_DATA', disabled: false, value: "0"},
										{ label: 'PRBS7', disabled: false, value: "1"},
										{ label: 'BIN_CNTR_UP', disabled: false, value: "2"},
										{ label: 'BIN_CNTR_DOWN', disabled: false, value: "3"},
										{ label: 'CONST_PATTERN', disabled: false, value: "4"},
										{ label: 'CONST_PATTERN_INV', disabled: false, value: "5"},
										{ label: 'DL_DATA_LOOPBACK', disabled: false, value: "6"}],	

			DataSource_val0: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE1']['ULG0DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DataSource_val1: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE1']['ULG1DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DataSource_val2: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE2']['ULG2DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DataSource_val3: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE2']['ULG3DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DataSource_val4: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE3']['ULG4DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DataSource_val5: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE3']['ULG5DATASOURCE'][Number(props.lpGBTNum)-1]), 

			DLDataSource : [{ label: 'LINK_DATA', disabled: false, value: "0"},
											{ label: 'PRBS7', disabled: false, value: "1"},
											{ label: 'BIN_CNTR_UP', disabled: false, value: "2"},
											{ label: 'CONST_PATTERN', disabled: false, value: "3"}],

			DLDataSource_val0: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE5']['DLG0DATASOURCE'][Number(props.lpGBTNum)-1]),  
			DLDataSource_val1: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE5']['DLG1DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DLDataSource_val2: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE5']['DLG2DATASOURCE'][Number(props.lpGBTNum)-1]), 
			DLDataSource_val3: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE5']['DLG3DATASOURCE'][Number(props.lpGBTNum)-1]), 

			LDDataSource : [{ label: 'Data from serializer', disabled: false, value: "0"}, 
											{ label: 'Data resampled from CD', disabled: false, value: "1"}, 
											{ label: 'Equalizer output data', disabled: false, value: "2"}, 
											{ label: 'reserved', disabled: false, value: "3"}],

			LDDataSource_val: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE1']['LDDATASOURCE'][Number(props.lpGBTNum)-1]),  
 
			constPatternOne: "10101010", constPatternTwo: "10101010", constPatternThree: "10101010", constPatternFour: "10101010",

			BERT_coarse: [{ label: 'DISABLED', disabled: false, value: "0"},
										{ label: 'ULDG0', disabled: false, value: "1"},
										{ label: 'ULDG1', disabled: false, value: "2"},
										{ label: 'ULDG2', disabled: false, value: "3"},
										{ label: 'ULDG3', disabled: false, value: "4"},
										{ label: 'ULDG4', disabled: false, value: "5"},
										{ label: 'ULDG5', disabled: false, value: "6"},
										{ label: 'ULDG6', disabled: false, value: "7"},
										{ label: 'ULEC', disabled: false, value: "8"},
										{ label: 'DLDG0', disabled: false, value: "9"},
										{ label: 'DLDG1', disabled: false, value: "10"},
										{ label: 'DLDG2', disabled: false, value: "11"},
										{ label: 'DLDG3', disabled: false, value: "12"},
										{ label: 'DLEC', disabled: false, value: "13"},
										{ label: 'DLFRAME', disabled: false, value: "14"},],
			BERT_coarse_val: "0",

			BERT_fine: [{ label: 'UL_PRBS7_DR1_CH0', disabled: false, value: "0"},
									{ label: 'UL_PRBS7_DR1_CH1', disabled: false, value: "1"},
									{ label: 'UL_PRBS7_DR1_CH2', disabled: false, value: "2"},
									{ label: 'UL_PRBS7_DR1_CH3', disabled: false, value: "3"},
									{ label: 'UL_PRBS7_DR2_CH0', disabled: false, value: "4"},
									{ label: 'UL_PRBS7_DR2_CH2', disabled: false, value: "5"},
									{ label: 'UL_PRBS7_DR3_CH0', disabled: false, value: "6"},
									{ label: 'UL_FIXED', disabled: false, value: "7"},],
			BERT_fine_val: "0",

			BERT_measTime: [{ label: '32', disabled: false, value: "0"},
											{ label: '128', disabled: false, value: "1"},
											{ label: '512', disabled: false, value: "2"},
											{ label: '2k', disabled: false, value: "3"},
											{ label: '8k', disabled: false, value: "4"},
											{ label: '32k', disabled: false, value: "5"}, 
											{ label: '128k', disabled: false, value: "6"},
											{ label: '512k', disabled: false, value: "7"},
											{ label: '2M', disabled: false, value: "8"},
											{ label: '8M', disabled: false, value: "9"},
											{ label: '32M', disabled: false, value: "10"},
											{ label: '128M', disabled: false, value: "11"},
											{ label: '512M', disabled: false, value: "12"},
											{ label: '2G', disabled: false, value: "13"},
											{ label: '8G', disabled: false, value: "14"},
											{ label: '32G', disabled: false, value: "15"},],
			BERTMeasTime_val: "0",
			isLoading: false,

			ULGECDataSource: [{ label: 'EPORTRX_DATA', disabled: false, value: "0"}],
			ULGECDataSource_val: "0",

			ULSerTestPattern: [{ label: 'DATA', disabled: false, value: "0"},
												{ label: 'PRBS7', disabled: false, value: "1"},
												{ label: 'PRBS15', disabled: false, value: "2"},
												{ label: 'PRBS23', disabled: false, value: "3"},
												{ label: 'PRBS31', disabled: false, value: "4"},
												{ label: 'CLK5G12', disabled: false, value: "5"},
												{ label: 'CLK2G56', disabled: false, value: "6"},
												{ label: 'CLK5G12', disabled: false, value: "7"},
												{ label: 'CLK1G28', disabled: false, value: "8"},
												{ label: 'CLK40M', disabled: false, value: "9"},
												{ label: 'DLFRAME_10G24', disabled: false, value: "10"},
												{ label: 'DLFRAME_5G12', disabled: false, value: "11"},
												{ label: 'DLFRAME_2G56', disabled: false, value: "12"},
												{ label: 'CONST_PATTERN', disabled: false, value: "13"},],

			ULSerTestPattern_val: String(props.ConfigurationFile["lpgbt"]['ULDATASOURCE0']['ULSERTESTPATTERN'][Number(props.lpGBTNum)-1]), 

			variantPattern: 'primary', 
			isModalOpen: false,
			totalErrors: "X",
			totalBits: "Y",
			CL: "Z",
			isLoading: false,
	};

	this.handleForm1 = (ULGECDataSource_val) => {
		const memory = this.state.ULGECDataSource_val;
		this.setState({ ULGECDataSource_val });
		console.log(ULGECDataSource_val);
		this.props.sendSetting('ULDataSource0','ULGECDataSource', ULGECDataSource_val).catch((err) => {console.log(err); this.setState({ ULGECDataSource_val: memory });});
	};

	this.handleForm2 = (ULSerTestPattern_val) => {
		const memory = this.state.ULSerTestPattern_val;
		this.setState({ ULSerTestPattern_val });
		console.log(ULSerTestPattern_val);
		this.sendSettingTESTPATTERN('ULDataSource0','ULSERTESTPATTERN', ULSerTestPattern_val).catch((err) => {console.log(err); this.setState({ ULSerTestPattern_val: memory });});
	};

	this.handleForm3 = (LDDataSource_val) => {
		const memory = this.state.LDDataSource_val;
		this.setState({ LDDataSource_val });
		console.log(LDDataSource_val);
		this.props.sendSetting('ULDataSource1','LDDataSource', LDDataSource_val).catch((err) => {console.log(err); this.setState({ LDDataSource_val: memory });});
	};


	this.handleFormDL0 = (DLDataSource_val0) => {
		const memory = this.state.DLDataSource_val0;
		this.setState({ DLDataSource_val0 });
		console.log(DLDataSource_val0);
		this.props.sendSetting('ULDataSource5','DLG0DataSource', DLDataSource_val0).catch((err) => {console.log(err); this.setState({ DLDataSource_val0: memory });});
	};
	this.handleFormDL1 = (DLDataSource_val1) => {
		const memory = this.state.DLDataSource_val1;
		this.setState({ DLDataSource_val1 });
		console.log(DLDataSource_val1);
		this.props.sendSetting('ULDataSource5','DLG1DataSource', DLDataSource_val1).catch((err) => {console.log(err); this.setState({ DLDataSource_val1: memory });});
	};
	this.handleFormDL2 = (DLDataSource_val2) => {
		const memory = this.state.DLDataSource_val2;
		this.setState({ DLDataSource_val2 });
		console.log(DLDataSource_val2);
		this.props.sendSetting('ULDataSource5','DLG2DataSource', DLDataSource_val2).catch((err) => {console.log(err); this.setState({ DLDataSource_val2: memory });});
	};
	this.handleFormDL3 = (DLDataSource_val3) => {
		const memory = this.state.DLDataSource_val3;
		this.setState({ DLDataSource_val3 });
		console.log(DLDataSource_val3);
		this.props.sendSetting('ULDataSource5','DLG3DataSource', DLDataSource_val3).catch((err) => {console.log(err); this.setState({ DLDataSource_val3: memory });});
	};


	this.handleFormUL0 = (DataSource_val0) => {
		const memory = this.state.DataSource_val0;
		this.setState({ DataSource_val0 });
		console.log(DataSource_val0);
		this.props.sendSetting('ULDataSource1','ULG0DataSource', DataSource_val0).catch((err) => {console.log(err); this.setState({ DataSource_val0: memory });});
	};
	this.handleFormUL1 = (DataSource_val1) => {
		const memory = this.state.DataSource_val1;
		this.setState({ DataSource_val1 });
		console.log(DataSource_val1);
		this.props.sendSetting('ULDataSource1','ULG1DataSource', DataSource_val1).catch((err) => {console.log(err); this.setState({ DataSource_val1: memory });});
	};
	this.handleFormUL2 = (DataSource_val2) => {
		const memory = this.state.DataSource_val2;
		this.setState({ DataSource_val2 });
		console.log(DataSource_val2);
		this.props.sendSetting('ULDataSource2','ULG2DataSource', DataSource_val2).catch((err) => {console.log(err); this.setState({ DataSource_val2: memory });});
	};
	this.handleFormUL3 = (DataSource_val3) => {
		const memory = this.state.DataSource_val3;
		this.setState({ DataSource_val3 });
		console.log(DataSource_val3);
		this.props.sendSetting('ULDataSource2','ULG3DataSource', DataSource_val3).catch((err) => {console.log(err); this.setState({ DataSource_val3: memory });});
	};
	this.handleFormUL4 = (DataSource_val4) => {
		const memory = this.state.DataSource_val4;
		this.setState({ DataSource_val4 });
		console.log(DataSource_val4);
		this.props.sendSetting('ULDataSource3','ULG4DataSource', DataSource_val4).catch((err) => {console.log(err); this.setState({ DataSource_val4: memory });});
	};
	this.handleFormUL5 = (DataSource_val5) => {
		const memory = this.state.DataSource_val5;
		this.setState({ DataSource_val5 });
		console.log(DataSource_val5);
		this.props.sendSetting('ULDataSource3','ULG5DataSource', DataSource_val5).catch((err) => {console.log(err); this.setState({ DataSource_val5: memory });});
	};



	this.BERTSource1 = (BERT_coarse_val) => {
		const memory = this.state.BERT_coarse_val;
		this.setState({ BERT_coarse_val });
		console.log(BERT_coarse_val);
		let newData = (BERT_coarse_val << 4) | (this.state.BERT_fine_val)
		this.props.sendSetting('BERTSource','BERTSource', newData.toString()).catch((err) => {console.log(err); this.setState({ BERT_coarse_val: memory });});
	};

	this.BERTSource2 = (BERT_fine_val) => {
		const memory = this.state.BERT_fine_val;
		this.setState({ BERT_fine_val });
		console.log(BERT_fine_val);
		let newData = (this.state.BERT_coarse_val << 4) | (BERT_fine_val)
		this.props.sendSetting('BERTSource','BERTSource', newData.toString()).catch((err) => {console.log(err); this.setState({ BERT_fine_val: memory });});
	};

	this.BERTConfig = (BERTMeasTime_val) => {
		const memory = this.state.BERTMeasTime_val;
		this.setState({ BERTMeasTime_val });
		console.log(BERTMeasTime_val);
		this.props.sendSetting('BERTConfig','BERTMeasTime', BERTMeasTime_val).catch((err) => {console.log(err); this.setState({ BERTMeasTime_val: memory });});
	};


	this.BERT_Test = () => {
		console.log("Starting BERT!")
		this.setState({
			isLoading: true
		});
		const body = {
			device: this.props.device,
		}
		fetch(`${this.props.backendUrl}/BERTScan`, getPostBody(body)).then(
    	response => checkResponse(response)).then(
	    data =>  {console.log(data); this.setState({ isLoading: false	})}).catch(err => {this.setState({ isLoading: false	}); console.log(err)});

	};

	this.handleTextInputChange1 = constPatternOne => {
		this.setState({ constPatternOne });
		if (constPatternOne == "") { this.setState({ variantPattern : "danger" }); 
		} else { this.setState({ variantPattern : "warning" }); } 
		if (constPatternOne.length > 8) { this.setState({ variantPattern : "danger" }); 
		} else { this.setState({ variantPattern : "warning" }); } 		
	};
	this.handleTextInputChange2 = constPatternTwo => {
		this.setState({ constPatternTwo }); 
		if (constPatternTwo == "") {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
		if (constPatternTwo.length > 8) {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
	};
	this.handleTextInputChange3 = constPatternThree => {
		this.setState({ constPatternThree });
		if (constPatternThree == "") {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
		if (constPatternThree.length > 8) {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
	};
	this.handleTextInputChange4 = constPatternFour => {
		this.setState({ constPatternFour }); 
		if (constPatternFour == "") {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
		if (constPatternFour.length > 8) {this.setState({ variantPattern : "danger" });
		} else { this.setState({ variantPattern : "warning" }); }
	};

/*	this.sendConstPattern = () => {
		this.setState({ variantPattern : "primary" });
		console.log(this.state.constPatternOne);
		this.props.sendSetting('DPDataPattern3','DPDATAPATTERN', this.state.constPatternOne);
		console.log(this.state.constPatternTwo);
		this.props.sendSetting('DPDataPattern2','DPDATAPATTERN', this.state.constPatternTwo);
		console.log(this.state.constPatternThree);
		this.props.sendSetting('DPDataPattern1','DPDATAPATTERN', this.state.constPatternThree);
		console.log(this.state.constPatternFour);
		this.props.sendSetting('DPDataPattern0','DPDATAPATTERN', this.state.constPatternFour);
	};*/

	this.sendConstPattern = () => {
		if (this.state.variantPattern == "danger") {

			console.log("Not a valid constant pattern!!!");

		} else {

			this.setState({ variantPattern : "primary" });
			const body = {
				device: 'lpgbt' + this.props.lpGBTNum,
				settingName: 'DPDATAPATTERN',
				registerName1: 'DPDataPattern3',
				newValue1: this.state.constPatternOne,
				registerName2: 'DPDataPattern2',
				newValue2: this.state.constPatternTwo,
				registerName3: 'DPDataPattern1',
				newValue3: this.state.constPatternThree,
				registerName4: 'DPDataPattern0',
				newValue4: this.state.constPatternFour,
			};
			console.log(body);
			fetch(`${this.props.backendUrl}/sendConstPattern`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {console.log(data)}).catch(err => {this.setState({ variantPattern : "warning" })});//.then(([data1, data2]) => {console.log(data1); console.log(data2)});
			};		

		}


  this.handleModalToggle = () => {
    this.setState(({ isModalOpen }) => ({
      isModalOpen: !isModalOpen
    }));
  };

  this.handleModalToggleClose = () => {
    this.setState(({ isModalOpen }) => ({
      isModalOpen: !isModalOpen
    }));
    this.sendSettingTESTPATTERN('ULDataSource0','ULSERTESTPATTERN', "0").catch(err => {console.log(err)});
  };

  this.sendSettingTESTPATTERN = (registerName,settingName,newValue) => {
		const body = {
			device: 'lpgbt' + props.lpGBTNum,
			registerName: registerName,
			settingName: settingName,
			newValue: newValue,
		};
		console.log(body);
		return fetch(`${this.props.backendUrl}/sendRegisterNoReadBack`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {console.log(data)});
	  };

  this.ExecuteBERT = () => {
  	this.setState({	isLoading: true });
		const body = {
			device: 'lpgbt' + props.lpGBTNum
		};
		console.log(body);
		fetch(`${this.props.backendUrl}/BERTScan`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {
			console.log(data); 
			this.setState({totalErrors : data['BERT_error_count']}); this.setState({totalBits : data['total_bits']}); this.setState({CL : data['BER_limit']});
			this.setState({	isLoading: false });
		}).catch(err => {this.setState({ isLoading: false	}); console.log(err)});
  };
	};

	render (){
	return (

	<React.Fragment>

	<Panel variant="bordered">
		<PanelHeader>
			<Bullseye>
				<Flex>
					<FlexItem>
						<Bullseye>
							<TextContent>
								<Text component={TextVariants.h3}>Uplink test patterns</Text>
							</TextContent>
						</Bullseye>
					</FlexItem>

					<FlexItem>
						<Bullseye>
							<Button variant="primary" onClick={this.handleModalToggle}>
								<Tooltip content={ <div>BERT test. </div> }>
									<TextContent>
										<Text component={TextVariants.h3}>Settings</Text>
									</TextContent>
								</Tooltip>
							</Button>
						</Bullseye>
					</FlexItem>

				</Flex>

        <Modal
        	variant={ModalVariant.medium}
          title="Uplink test patterns"
          isOpen={this.state.isModalOpen}
          onClose={this.handleModalToggleClose}
          actions={[
            <Button key="close" variant="primary" onClick={this.handleModalToggleClose}>
              Close
            </Button>,
          ]}
        >
        	<Bullseye>
	        	<Flex>
		        	<FlexItem>
			        	<Bullseye>

          					<Flex direction={{ default: 'column' }}>

											<Panel variant="bordered">
												
												<FlexItem flex={{ default: 'flex_1' }}>
													<Panel variant="bordered">
													  <PanelMain>
														  <PanelMainBody>

																<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>

																	<Bullseye>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<TextContent>
																				<Text component={TextVariants.h3}>Serializer data source</Text>
																			</TextContent>
																		</ FlexItem>
																	</Bullseye>

																	<Flex justifyContent={{ default: 'justifyContentFlexCenter' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="ULGECDataSource" choices={this.state.ULGECDataSource} value={this.state.ULGECDataSource_val} onChange={this.handleForm1}/>
																			<Tooltip content={<div>No read-back required. Don't use for debugging!</div>}>
																				<HelperUplinkTestPatterns name="ULSerTestPattern" choices={this.state.ULSerTestPattern} value={this.state.ULSerTestPattern_val} onChange={this.handleForm2}/>
																			</Tooltip>
																		</ FlexItem>
																	</Flex>

																</Flex >

															</PanelMainBody>
														</PanelMain>
													</Panel>
												</FlexItem>

											<Panel variant="bordered">
										  <PanelMain>
										  <PanelMainBody>

												<Flex  >
														<FlexItem flex={{ default: 'flex_1' }} justifyContent={{ default: 'justifyContentFlexCenter' }}> 
															<Bullseye>
																<TextContent>
																	<Text component={TextVariants.h3}>CONST PATTERN (4 bytes)</Text>
																</TextContent>
															</Bullseye>
														</FlexItem>

														<FlexItem flex={{ default: 'flex_1' }}>
															<Bullseye>
																<Tooltip content={"Yellow button means that the constant pattern has been modified! Red button means that the pattern is incorrect! "}>
																	<Button id="const_pattern_send" variant={this.state.variantPattern} onClick={() => {this.sendConstPattern()}}>
																		Send
																	</Button>
																</Tooltip>
															</Bullseye>
														</ FlexItem>
												
												

													
													<Flex>
														<FlexItem spacer={{ default: 'spacerSm' }} flex={{ default: 'flex_1' }}>
															<Bullseye>
																<TextInput id="constPattern1" name="constPattern1" value={this.state.constPatternOne} onChange={this.handleTextInputChange1}/>
															</Bullseye>
														</ FlexItem>

														<FlexItem spacer={{ default: 'spacerSm' }} flex={{ default: 'flex_1' }}>
															<Bullseye>
																<TextInput id="constPattern2" name="constPattern2" value={this.state.constPatternTwo} onChange={this.handleTextInputChange2}/>
															</Bullseye>
														</ FlexItem>

														<FlexItem spacer={{ default: 'spacerSm' }} flex={{ default: 'flex_1' }}>
															<Bullseye>
																<TextInput id="constPattern3" name="constPattern3" value={this.state.constPatternThree} onChange={this.handleTextInputChange3}/>
															</Bullseye>
														</ FlexItem>

														<FlexItem spacer={{ default: 'spacerSm' }} flex={{ default: 'flex_1' }}>
															<Bullseye>
																<TextInput id="constPattern4" name="constPattern4" value={this.state.constPatternFour} onChange={this.handleTextInputChange4}/>
															</Bullseye>
														</ FlexItem>
													</Flex>
													

												</Flex >

											</PanelMainBody>
											</PanelMain>
											</Panel>


											<Panel variant="bordered">
										  <PanelMain>
										  <PanelMainBody>

												<Flex direction={{ default: 'column' }}>

													<Bullseye>
														<FlexItem flex={{ default: 'flex_2' }}>
															<TextContent>
																<Text component={TextVariants.h3}>ULDataSource 1, 2, 3</Text>
															</TextContent>
														</ FlexItem>
													</Bullseye>

													<FlexItem flex={{ default: 'flex_2' }}>
													<HelperUplinkTestPatterns name="LDDataSource" choices={this.state.LDDataSource} value={this.state.LDDataSource_val} onChange={this.handleForm3}/>
													<HelperUplinkTestPatterns name="ULG1DataSource" choices={this.state.DataSource} value={this.state.DataSource_val1} onChange={this.handleFormUL1}/>
													<HelperUplinkTestPatterns name="ULG0DataSource" choices={this.state.DataSource} value={this.state.DataSource_val0} onChange={this.handleFormUL0}/>

													<HelperUplinkTestPatterns name="ULG3DataSource" choices={this.state.DataSource} value={this.state.DataSource_val3} onChange={this.handleFormUL3}/>
													<HelperUplinkTestPatterns name="ULG2DataSource" choices={this.state.DataSource} value={this.state.DataSource_val2} onChange={this.handleFormUL2}/>

													<HelperUplinkTestPatterns name="ULG5DataSource" choices={this.state.DataSource} value={this.state.DataSource_val5} onChange={this.handleFormUL5}/>
													<HelperUplinkTestPatterns name="ULG4DataSource" choices={this.state.DataSource} value={this.state.DataSource_val4} onChange={this.handleFormUL4}/>
													</FlexItem>

												</Flex >

											</PanelMainBody>
											</PanelMain>
											</Panel>



											<Panel variant="bordered">
										  <PanelMain>
										  <PanelMainBody>

												<Flex direction={{ default: 'column' }}>
													<Bullseye>
														<FlexItem flex={{ default: 'flex_2' }}>
															<TextContent>
																<Text component={TextVariants.h3}>Downlink test patterns</Text>
															</TextContent>
														</ FlexItem>
													</Bullseye>

													<HelperUplinkTestPatterns name="DLG3DataSource" choices={this.state.DLDataSource} value={this.state.DLDataSource_val3} onChange={this.handleFormDL3}/>
													<HelperUplinkTestPatterns name="DLG2DataSource" choices={this.state.DLDataSource} value={this.state.DLDataSource_val2} onChange={this.handleFormDL2}/>
													<HelperUplinkTestPatterns name="DLG1DataSource" choices={this.state.DLDataSource} value={this.state.DLDataSource_val1} onChange={this.handleFormDL1}/>
													<HelperUplinkTestPatterns name="DLG0DataSource" choices={this.state.DLDataSource} value={this.state.DLDataSource_val0} onChange={this.handleFormDL0}/>

												</Flex >

											</PanelMainBody>
											</PanelMain>
											</Panel>


											<Panel variant="bordered">
										  <PanelMain>
										  <PanelMainBody>

												<Grid >

													<GridItem span={3}>
														<Bullseye>
															<Button spinnerAriaValueText={this.state.isLoading ? 'Loading' : undefined}	isLoading={this.state.isLoading} id="downlinkTestPattern" variant="primary" onClick={this.ExecuteBERT}>
																<h3> BER Test </h3>
															</Button>
														</Bullseye>
													</ GridItem>

													<GridItem span={5}>
														<Bullseye>
															Errors: {this.state.totalErrors} / {this.state.totalBits}
														</Bullseye>
													</GridItem>

													<GridItem span={4}>
														<Bullseye>
															95% CL: {this.state.CL}
														</Bullseye>
													</GridItem>

													<HelperUplinkTestPatterns name="BERTSource (coarse)" choices={this.state.BERT_coarse} value={this.state.BERT_coarse_val} onChange={this.BERTSource1}/>
													<HelperUplinkTestPatterns name="BERTSource (fine)" choices={this.state.BERT_fine} value={this.state.BERT_fine_val} onChange={this.BERTSource2}/>
													<HelperUplinkTestPatterns name="BERTMeasTime (clock cycles)" choices={this.state.BERT_measTime} value={this.state.BERTMeasTime_val} onChange={this.BERTConfig}/>

												</Grid >

											</PanelMainBody>
											</PanelMain>
											</Panel>


											</Panel>

											</Flex>
			          
			          </Bullseye>
		          </FlexItem>
	          </Flex>
	        </Bullseye>
      	</Modal>

   		</Bullseye>
		</PanelHeader>
	</Panel>



	</React.Fragment>
);};
}

export {TestPatterns};

