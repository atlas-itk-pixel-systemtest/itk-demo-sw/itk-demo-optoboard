import React from "react";
import {
  Button,
  Checkbox,
  InputGroup,
  InputGroupText,
  Dropdown,
  DropdownToggle,
  DropdownItem,
  FormSelect, 
  FormSelectOption, 
  FormSelectOptionGroup,
  Text,
  TextInput,
  TextContent,
  TextVariants,
  Form,
  FormGroup,
  PanelMainBody,
  PanelMain,
  Panel,
  PanelHeader,
  SidebarContent,
  Sidebar,
  Flex, 
  FlexItem,
  Grid,
  GridItem,
  Bullseye,
  ModalVariant,
  Modal,
  Tooltip,
} from "@patternfly/react-core";

import { getPostBody, checkResponse } from '../utils/utility-functions'
import { FormSelectInput, FormSelectInputNewer } from "../Widgets/FormSelectInput";


class HelperUplinkTestPatterns extends React.Component {
	constructor(props) {
		super(props);
	};

	render (){
	return (
	<React.Fragment>

	<Flex spaceItems={{ default: 'spaceItemsNone' }}>
		<FlexItem flex={{ default: 'flex_1' }}>
			<Bullseye>
				<h4> {this.props.name} </h4>
			</Bullseye>
		</FlexItem>
	
		<FlexItem flex={{ default: 'flex_1' }}>
			<Bullseye>
				<FormSelectInputNewer id={this.props.name} items={this.props.choices} value={this.props.value} onChange={this.props.onChange}/>
			</Bullseye>
		</FlexItem>
	</Flex>

	</React.Fragment>
);};
};



class TestPatternsSlave extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			DataSource : [{ label: 'EPORTRX_DATA', value: '0', disabled: false},
										{ label: 'PRBS7', value: '1', disabled: false},
										{ label: 'BIN_CNTR_UP', value: '2', disabled: false},
										{ label: 'BIN_CNTR_DOWN', value: '3', disabled: false},
										{ label: 'CONST_PATTERN', value: '4', disabled: false},
										{ label: 'CONST_PATTERN_INV', value: '5', disabled: false},
										{ label: 'DL_DATA_LOOPBACK', value: '6', disabled: false}],
			DataSource_val0: "0", DataSource_val1: "0", DataSource_val2: "0", DataSource_val3: "0", DataSource_val4: "0", DataSource_val5: "0", 

			LDDataSource : [{ label: 'Data from serializer', value: '0', disabled: false}, 
											{ label: 'Data resampled from CD', value: '1', disabled: false}, 
											{ label: 'Equalizer output data', value: '2', disabled: false}, 
											{ label: 'reserved', value: '3', disabled: false}],
			LDDataSource_val: "0", 

			constPatternOne: "10101010", constPatternTwo: "10101010", constPatternThree: "10101010", constPatternFour: "10101010",

			BERT_coarse: [{ label: 'DISABLED', value: '0', disabled: false},
										{ label: 'ULDG0', value: '1', disabled: false},
										{ label: 'ULDG1', value: '2', disabled: false},
										{ label: 'ULDG2', value: '3', disabled: false},
										{ label: 'ULDG3', value: '4', disabled: false},
										{ label: 'ULDG4', value: '5', disabled: false},
										{ label: 'ULDG5', value: '6', disabled: false},
										{ label: 'ULDG6', value: '7', disabled: false},
										{ label: 'ULEC', value: '8', disabled: false},
										{ label: 'DLDG0', value: '9', disabled: false},
										{ label: 'DLDG1', value: '10', disabled: false},
										{ label: 'DLDG2', value: '11', disabled: false},
										{ label: 'DLDG3', value: '12', disabled: false},
										{ label: 'DLEC', value: '13', disabled: false},
										{ label: 'DLFRAME', value: '14', disabled: false},],
			BERT_coarse_val: "0",

			BERT_fine: [{ label: 'UL_PRBS7_DR1_CH0', value: '0', disabled: false},
									{ label: 'UL_PRBS7_DR1_CH1', value: '1', disabled: false},
									{ label: 'UL_PRBS7_DR1_CH2', value: '2', disabled: false},
									{ label: 'UL_PRBS7_DR1_CH3', value: '3', disabled: false},
									{ label: 'UL_PRBS7_DR2_CH0', value: '4', disabled: false},
									{ label: 'UL_PRBS7_DR2_CH2', value: '5', disabled: false},
									{ label: 'UL_PRBS7_DR3_CH0', value: '6', disabled: false},
									{ label: 'UL_FIXED', value: '7', disabled: false},],
			BERT_fine_val: "0",

			BERT_measTime: [{ label: '32', value: '0', disabled: false},
											{ label: '128', value: '1', disabled: false},
											{ label: '512', value: '2', disabled: false},
											{ label: '2k', value: '3', disabled: false},
											{ label: '8k', value: '4', disabled: false},
											{ label: '32k', value: '5', disabled: false}, 
											{ label: '128k', value: '6', disabled: false},
											{ label: '512k', value: '7', disabled: false},
											{ label: '2M', value: '8', disabled: false},
											{ label: '8M', value: '9', disabled: false},
											{ label: '32M', value: '10', disabled: false},
											{ label: '128M', value: '11', disabled: false},
											{ label: '512M', value: '12', disabled: false},
											{ label: '2G', value: '13', disabled: false},
											{ label: '8G', value: '14', disabled: false},
											{ label: '32G', value: '15', disabled: false},],
			BERTMeasTime_val: "0",

			ULGECDataSource: [{ label: 'EPORTRX_DATA', value: '0', disabled: false}],
			ULGECDataSource_val: "0",

			ULSerTestPattern: [{ label: 'DATA', value: '0', disabled: false},
												{ label: 'PRBS7', value: '1', disabled: false},
												{ label: 'PRBS15', value: '2', disabled: false},
												{ label: 'PRBS23', value: '3', disabled: false},
												{ label: 'PRBS31', value: '4', disabled: false},
												{ label: 'CLK5G12', value: '5', disabled: false},
												{ label: 'CLK2G56', value: '6', disabled: false},
												{ label: 'CLK5G12', value: '7', disabled: false},
												{ label: 'CLK1G28', value: '8', disabled: false},
												{ label: 'CLK40M', value: '9', disabled: false},
												{ label: 'DLFRAME_10G24', value: '10', disabled: false},
												{ label: 'DLFRAME_5G12', value: '11', disabled: false},
												{ label: 'DLFRAME_2G56', value: '12', disabled: false},
												{ label: 'CONST_PATTERN', value: '13', disabled: false},],
			ULSerTestPattern_val: "0",
			variantPattern: 'primary',	
			totalErrors: "X",
			totalBits: "Y",
			CL: "Z",
			isLoading: false,
	};

	this.handleTextInputChange1 = constPatternOne => {this.setState({ constPatternOne });
																										(constPatternOne == "") ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										(constPatternOne.length > 8) ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" }));
																										};
	this.handleTextInputChange2 = constPatternTwo => {this.setState({ constPatternTwo });
																										(constPatternTwo == "") ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										(constPatternTwo.length > 8) ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										};
	this.handleTextInputChange3 = constPatternThree => {this.setState({ constPatternThree });
																											(constPatternThree == "") ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" }));
																											(constPatternThree.length > 8) ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										 };
	this.handleTextInputChange4 = constPatternFour => {this.setState({ constPatternFour });
																										 (constPatternFour == "") ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										 (constPatternFour.length > 8) ? (this.setState({ variantPattern : "danger" })) : (this.setState({ variantPattern : "warning" })); 
																										};

	this.handleForm1 = (ULGECDataSource_val) => {
		const memory = this.state.ULGECDataSource_val;
		this.setState({ ULGECDataSource_val });
		console.log(ULGECDataSource_val);
		this.props.sendSetting('ULDataSource0','ULGECDataSource', ULGECDataSource_val).catch(err => {this.setState({ULGECDataSource_val : memory})});
	};

	this.handleForm2 = (ULSerTestPattern_val) => {
		const memory = this.state.ULSerTestPattern_val;
		this.setState({ ULSerTestPattern_val });
		console.log(ULSerTestPattern_val);
		this.props.sendSettingTESTPATTERN('ULDataSource0','ULSerTestPattern', ULSerTestPattern_val).catch(err => {this.setState({ULSerTestPattern_val : memory})});
	};

	this.handleForm3 = (LDDataSource_val) => {
		const memory = this.state.LDDataSource_val;
		this.setState({ LDDataSource_val });
		console.log(LDDataSource_val);
		this.props.sendSetting('ULDataSource1','LDDataSource', LDDataSource_val).catch(err => {this.setState({LDDataSource_val : memory})});
	};

	this.handleFormUL0 = (DataSource_val0) => {
		const memory = this.state.DataSource_val0;
		this.setState({ DataSource_val0 });
		console.log(DataSource_val0);
		this.props.sendSetting('ULDataSource1','ULG0DataSource', DataSource_val0).catch(err => {this.setState({DataSource_val0 : memory})});
	};

	this.handleFormUL1 = (DataSource_val1) => {
		const memory = this.state.DataSource_val1;
		this.setState({ DataSource_val1 });
		console.log(DataSource_val1);
		this.props.sendSetting('ULDataSource1','ULG1DataSource', DataSource_val1).catch(err => {this.setState({DataSource_val1 : memory})});
	};

	this.handleFormUL2 = (DataSource_val2) => {
		const memory = this.state.DataSource_val2;
		this.setState({ DataSource_val2 });
		console.log(DataSource_val2);
		this.props.sendSetting('ULDataSource2','ULG2DataSource', DataSource_val2).catch(err => {this.setState({DataSource_val2 : memory})});
	};
	this.handleFormUL3 = (DataSource_val3) => {
		const memory = this.state.DataSource_val3;
		this.setState({ DataSource_val3 });
		console.log(DataSource_val3);
		this.props.sendSetting('ULDataSource2','ULG3DataSource', DataSource_val3).catch(err => {this.setState({DataSource_val3 : memory})});
	};
	this.handleFormUL4 = (DataSource_val4) => {
		const memory = this.state.DataSource_val4;
		this.setState({ DataSource_val4 });
		console.log(DataSource_val4);
		this.props.sendSetting('ULDataSource3','ULG4DataSource', DataSource_val4).catch(err => {this.setState({DataSource_val4 : memory})});
	};
	this.handleFormUL5 = (DataSource_val5) => {
		const memory = this.state.DataSource_val5;
		this.setState({ DataSource_val5 });
		console.log(DataSource_val5);
		this.props.sendSetting('ULDataSource3','ULG5DataSource', DataSource_val5).catch(err => {this.setState({DataSource_val5 : memory})});
	};

	this.BERTSource1 = (BERT_coarse_val) => {
		const memory = this.state.BERT_coarse_val;
		this.setState({ BERT_coarse_val });
		console.log(BERT_coarse_val);
		let newData = (BERT_coarse_val << 4) | (this.state.BERT_fine_val)
		this.props.sendSetting('BERTSource','BERTSource', newData.toString()).catch(err => {this.setState({BERT_coarse_val : memory})});
	};

	this.BERTSource2 = (BERT_fine_val) => {
		const memory = this.state.BERT_fine_val;
		this.setState({ BERT_fine_val });
		console.log(BERT_fine_val);
		let newData = (this.state.BERT_coarse_val << 4) | (BERT_fine_val)
		this.props.sendSetting('BERTSource','BERTSource', newData.toString()).catch(err => {this.setState({BERT_fine_val : memory})});
	};

	this.BERTConfig = (BERTMeasTime_val) => {
		const memory = this.state.BERTMeasTime_val;
		this.setState({ BERTMeasTime_val });
		console.log(BERTMeasTime_val);
		this.props.sendSetting('BERTConfig','BERTMeasTime', BERTMeasTime_val).catch(err => {this.setState({BERTMeasTime_val : memory})});
	};

/*
	this.sendConstPattern = () => {
		this.setState({ variantPattern : "primary" });
		console.log(this.state.constPatternOne);
		this.props.sendSetting('DPDataPattern3','', this.state.constPatternOne);
		console.log(this.state.constPatternTwo);
		this.props.sendSetting('DPDataPattern2','', this.state.constPatternTwo);
		console.log(this.state.constPatternThree);
		this.props.sendSetting('DPDataPattern1','', this.state.constPatternThree);
		console.log(this.state.constPatternFour);
		this.props.sendSetting('DPDataPattern0','', this.state.constPatternFour);
	};
*/

	this.sendConstPattern = () => {
		if (this.state.variantPattern == "danger") {

			console.log("Not a valid constant pattern!!!");

		} else {

		this.setState({ variantPattern : "primary" });
		const body = {
			device: 'lpgbt' + this.props.lpGBTNum,
			settingName: 'DPDATAPATTERN',
			registerName1: 'DPDataPattern3',
			newValue1: this.state.constPatternOne,
			registerName2: 'DPDataPattern2',
			newValue2: this.state.constPatternTwo,
			registerName3: 'DPDataPattern1',
			newValue3: this.state.constPatternThree,
			registerName4: 'DPDataPattern0',
			newValue4: this.state.constPatternFour,
		};
		console.log(body);
		fetch(`${this.props.backendUrl}/sendConstPattern`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {console.log(data)}).catch(err => {this.setState({ variantPattern : "warning" })});//.then(([data1, data2]) => {console.log(data1); console.log(data2)});
		};
		};

  this.sendSettingTESTPATTERN = (registerName,settingName,newValue) => {
		const body = {
			device: 'lpgbt' + props.lpGBTNum,
			registerName: registerName,
			settingName: settingName,
			newValue: newValue,
		};
		console.log(body);
		return fetch(`${this.props.backendUrl}/sendRegisterNoReadBack`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {console.log(data)});
	  };

  this.handleModalToggle = () => {
    this.setState(({ isModalOpen }) => ({
      isModalOpen: !isModalOpen
    }));
  };

  this.handleModalToggleClose = () => {
    this.setState(({ isModalOpen }) => ({
      isModalOpen: !isModalOpen
    }));
    this.sendSettingTESTPATTERN('ULDataSource0','ULSERTESTPATTERN', "0").catch(err => {console.log(err)});
  };

  this.ExecuteBERT = () => {
  	this.setState({	isLoading: true });
		const body = {
			device: 'lpgbt' + props.lpGBTNum
		};
		console.log(body);
		fetch(`${this.props.backendUrl}/BERTScan`, getPostBody(body)).then((response) => checkResponse(response)).then((data) => {
			console.log(data);
			this.setState({totalErrors : data['BERT_error_count']}); this.setState({totalBits : data['total_bits']}); this.setState({CL : data['BER_limit']});
			this.setState({	isLoading: false });
		}).catch(err => {this.setState({ isLoading: false	}); console.log(err)});
  };

	};

	render (){
	return (

	<React.Fragment>

	<Panel variant="bordered">    
		<PanelHeader>
			<Flex direction={{ default: 'column' }} >
				<FlexItem>	
					<Bullseye>
						<TextContent>
							<Text component={TextVariants.h3}>Uplink test patterns</Text>
						</TextContent>
					</Bullseye>
				</FlexItem>

				<FlexItem>
					<Bullseye>
						<Button variant="primary" onClick={this.handleModalToggle}>
							<Tooltip content={ <div>BERT test. </div> }>
								<TextContent>
									<Text component={TextVariants.h3}>Settings</Text>
								</TextContent>
							</Tooltip>
						</Button>
					</Bullseye>
				</FlexItem>

		    <Modal
		    	variant={ModalVariant.medium}
		      title="Uplink test patterns - slaves"
		      isOpen={this.state.isModalOpen}
		      onClose={this.handleModalToggleClose}
		      actions={[
		        <Button key="close" variant="primary" onClick={this.handleModalToggleClose}>
		          Close
		        </Button>,
		      ]}
		    >
			    	<Flex>
			      	<FlexItem>
			        	<Bullseye> 
			  
										<Flex direction={{ default: 'column' }}>

											<FlexItem flex={{ default: 'flex_1' }}>

												<Panel variant="bordered">
										      <PanelMain>
										        <PanelMainBody>

															<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>
																<FlexItem flex={{ default: 'flex_1' }}>
																	<Bullseye>
																		<TextContent>
																			<Text component={TextVariants.h3}>Serializer data source</Text>
																		</TextContent>
																	</Bullseye>
																</ FlexItem>

																<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																	<FlexItem flex={{ default: 'flex_1' }}>
																		<Bullseye>
																			<HelperUplinkTestPatterns name="ULGECDataSource" choices={this.state.ULGECDataSource} value={this.state.ULGECDataSource_val} onChange={this.handleForm1}/>
																		</Bullseye>
																	</ FlexItem>
																	<FlexItem flex={{ default: 'flex_1' }}>
																		<Bullseye>
																			<Tooltip content={<div>No read-back required. Don't use for debugging!</div>}>
																				<HelperUplinkTestPatterns name="ULSerTestPattern" choices={this.state.ULSerTestPattern} value={this.state.ULSerTestPattern_val} onChange={this.handleForm2}/>
																			</Tooltip>
																		</Bullseye>
																	</ FlexItem>
																</Flex>
															</Flex>

														</PanelMainBody>
													</PanelMain>
												</Panel>

											</FlexItem>

											<FlexItem flex={{ default: 'flex_1' }}>

												<Panel variant="bordered">
										      <PanelMain>
										        <PanelMainBody>
															<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																<FlexItem flex={{ default: 'flex_1' }}>
																				<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																					<FlexItem flex={{ default: 'flex_1' }}>
																						<Bullseye>
																							<TextContent>
																								<Text component={TextVariants.h3}>CONST PATTERN (4 bytes)</Text>
																							</TextContent>
																						</Bullseye>
																					</ FlexItem>

																					<FlexItem flex={{ default: 'flex_1' }}>
																						<Bullseye>
																							<Button id="const_pattern_send" variant={this.state.variantPattern} onClick={() => {this.sendConstPattern()}}>
																								Send
																							</Button>
																						</Bullseye>
																					</ FlexItem>
																				</Flex>

																				<Flex>
																				
																						<FlexItem flex={{ default: 'flex_1' }} spacer={{ default: 'spacerSm' }}>
																							<Bullseye>
																								<TextInput id="constPattern1" name="constPattern1" value={this.state.constPatternOne} onChange={this.handleTextInputChange1}/>
																							</Bullseye>
																						</ FlexItem>

																						<FlexItem flex={{ default: 'flex_1' }} spacer={{ default: 'spacerSm' }}>
																							<Bullseye>
																								<TextInput id="constPattern2" name="constPattern2" value={this.state.constPatternTwo} onChange={this.handleTextInputChange2}/>
																							</Bullseye>
																						</ FlexItem>

																						<FlexItem flex={{ default: 'flex_1' }} spacer={{ default: 'spacerSm' }}>
																							<Bullseye>
																								<TextInput id="constPattern3" name="constPattern3" value={this.state.constPatternThree} onChange={this.handleTextInputChange3}/>
																							</Bullseye>
																						</ FlexItem>

																						<FlexItem flex={{ default: 'flex_1' }} spacer={{ default: 'spacerSm' }}>
																							<Bullseye>
																								<TextInput id="constPattern4" name="constPattern4" value={this.state.constPatternFour} onChange={this.handleTextInputChange4}/>
																							</Bullseye>
																						</ FlexItem>

																					
																				</Flex >
																</FlexItem>
															</Flex>

														</PanelMainBody>
													</PanelMain>
												</Panel>

											</FlexItem>

											<FlexItem flex={{ default: 'flex_1' }}>

												<Panel variant="bordered">
										      <PanelMain>
										        <PanelMainBody>

   	 													<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																<FlexItem flex={{ default: 'flex_1' }}>
														
																	<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<Bullseye>
																				<TextContent>
																					<Text component={TextVariants.h3}>ULDataSource1</Text>
																				</TextContent>
																			</Bullseye>
																		</ FlexItem>
																	</Flex >

																	<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="LDDataSource" choices={this.state.LDDataSource} value={this.state.LDDataSource_val} onChange={this.handleForm3}/>
																		</ FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="ULG1DataSource" choices={this.state.DataSource} value={this.state.DataSource_val1} onChange={this.handleFormUL1}/>
																		</ FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="ULG0DataSource" choices={this.state.DataSource} value={this.state.DataSource_val0} onChange={this.handleFormUL0}/>
																		</ FlexItem>
																	</Flex>

																</FlexItem>
															</Flex>

														</PanelMainBody>
													</PanelMain>
												</Panel>

											</FlexItem>

											<FlexItem flex={{ default: 'flex_1' }}>

														<Panel variant="bordered">
												      <PanelMain>
												        <PanelMainBody>
        										
        													<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																		
																			<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																				<FlexItem flex={{ default: 'flex_1' }}>
																					<Bullseye>
																						<TextContent>
																							<Text component={TextVariants.h3}>ULDataSource2</Text>
																						</TextContent>
																					</Bullseye>
																				</ FlexItem>
																			</Flex>

																			<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>
																				<FlexItem flex={{ default: 'flex_1' }}>
																					<HelperUplinkTestPatterns name="ULG3DataSource" choices={this.state.DataSource} value={this.state.DataSource_val3} onChange={this.handleFormUL3}/>
																				</ FlexItem>
																				<FlexItem flex={{ default: 'flex_1' }}>
																					<HelperUplinkTestPatterns name="ULG2DataSource" choices={this.state.DataSource} value={this.state.DataSource_val2} onChange={this.handleFormUL2}/>
																				</ FlexItem>
																			</Flex>

																		</FlexItem>
																	</Flex>
						
																</PanelMainBody>
															</PanelMain>
														</Panel>

												</FlexItem>


											<FlexItem flex={{ default: 'flex_1' }}>
												
												<Panel variant="bordered">
										      <PanelMain>
										        <PanelMainBody>
													
															<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																<FlexItem flex={{ default: 'flex_1' }}>
																	
																	<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<Bullseye>
																				<TextContent>
																					<Text component={TextVariants.h3}>ULDataSource3</Text>
																				</TextContent>
																			</Bullseye>
																		</ FlexItem>
																	</Flex>						

																	<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>	
																			<HelperUplinkTestPatterns name="ULG5DataSource" choices={this.state.DataSource} value={this.state.DataSource_val5} onChange={this.handleFormUL5}/>
																		</FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="ULG4DataSource" choices={this.state.DataSource} value={this.state.DataSource_val4} onChange={this.handleFormUL4}/>
																		</FlexItem>
																	</Flex>

																</FlexItem>
															</Flex>
													
														</PanelMainBody>
													</PanelMain>
												</Panel>

											</FlexItem>		
										
											<FlexItem flex={{ default: 'flex_1' }}>
												<Panel variant="bordered">
										      <PanelMain>
										        <PanelMainBody>
															<Flex>
																<FlexItem flex={{ default: 'flex_1' }}>

																	<Flex spaceItems={{ default: 'spaceItemsNone' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<Bullseye>
																				<Button spinnerAriaValueText={this.state.isLoading ? 'Loading' : undefined}	isLoading={this.state.isLoading}  id="downlinkTestPattern" variant="primary" onClick={this.ExecuteBERT}>
																					<h3> BER Test </h3>
																				</Button>
																			</Bullseye>
																		</ FlexItem>	
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<Bullseye>
																				Errors: {this.state.totalErrors} / {this.state.totalBits}
																			</Bullseye>
																		</ FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<Bullseye>
																				95% CL: {this.state.CL}
																			</Bullseye>
																		</FlexItem>

																	</Flex >

																	<Flex justifyContent={{ default: 'justifyContentFlexCenter' }} direction={{ default: 'column' }}>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="BERTSource (coarse)" choices={this.state.BERT_coarse} value={this.state.BERT_coarse_val} onChange={this.BERTSource1}/>
																		</ FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="BERTSource (fine)" choices={this.state.BERT_fine} value={this.state.BERT_fine_val} onChange={this.BERTSource2}/>
																		</ FlexItem>
																		<FlexItem flex={{ default: 'flex_1' }}>
																			<HelperUplinkTestPatterns name="BERTMeasTime (clock cycles)" choices={this.state.BERT_measTime} value={this.state.BERTMeasTime_val} onChange={this.BERTConfig}/>
																		</ FlexItem>
																	</Flex>
															
															</FlexItem>
														</Flex>			
														</PanelMainBody>
													</PanelMain>
												</Panel>

											</FlexItem>
										</Flex>
						
		     	 	  </Bullseye>
		        </FlexItem>
		      </Flex>

		  </Modal>

		</Flex>
	</PanelHeader>
	</Panel>
	</React.Fragment>
);};
}

export {TestPatternsSlave};

