import React, { useState, useEffect } from "react";
import {
  Button,
  PanelMainBody,
  PanelMain,
  Panel,
  Bullseye,
  Flex,
  FlexItem,
  Checkbox,
  Switch,
} from "@patternfly/react-core";
import { getPostBody, checkResponse } from "../utils/utility-functions";

function Config({ url, optoboardPosition, optoComponents }) {
  const [isLoading, setisLoading] = useState(false);
  const [switchState, setswitchState] = useState(false);
  const [CheckBox_lpgbt1, toggleBox_lpgbt1] = useState(
    optoComponents.lpgbt1 ? true : null
  );
  const [CheckBox_lpgbt2, toggleBox_lpgbt2] = useState(
    optoComponents.lpgbt2 ? true : null
  );
  const [CheckBox_lpgbt3, toggleBox_lpgbt3] = useState(
    optoComponents.lpgbt3 ? true : null
  );
  const [CheckBox_lpgbt4, toggleBox_lpgbt4] = useState(
    optoComponents.lpgbt4 ? true : null
  );
  const [CheckBox_gbcr1, toggleBox_gbcr1] = useState(
    optoComponents.gbcr1 ? true : null
  );
  const [CheckBox_gbcr2, toggleBox_gbcr2] = useState(
    optoComponents.gbcr2 ? true : null
  );
  const [CheckBox_gbcr3, toggleBox_gbcr3] = useState(
    optoComponents.gbcr3 ? true : null
  );
  const [CheckBox_gbcr4, toggleBox_gbcr4] = useState(
    optoComponents.gbcr4 ? true : null
  );
  const [buttonVariant, setbuttonVariant] = useState("primary");

  const configure = (url) => {
    let body = {};
    if (switchState) {
      body = {
        optoboardPosition: optoboardPosition,
        activeLpgbt:
          (CheckBox_lpgbt1 ? "1" : "0") +
          (CheckBox_lpgbt2 ? "1" : "0") +
          (CheckBox_lpgbt3 ? "1" : "0") +
          (CheckBox_lpgbt4 ? "1" : "0"),
        activeGbcr:
          (CheckBox_gbcr1 ? "1" : "0") +
          (CheckBox_gbcr2 ? "1" : "0") +
          (CheckBox_gbcr3 ? "1" : "0") +
          (CheckBox_gbcr4 ? "1" : "0"),
      };
    } else {
      body = {
        optoboardPosition: optoboardPosition,
        activeLpgbt: "None",
        activeGbcr: "None",
      };
    }
    setisLoading(true);
    console.log("Starting configuration of the Optoboard!");
    console.log(body);

    fetch(`${url}/configure`, getPostBody(body))
      .then((response) => checkResponse(response))
      .then((data) => {
        console.log(data);
        setisLoading(false);
        setbuttonVariant("primary");
      })
      .catch((err) => {
        setbuttonVariant("danger");
        console.log(err);
      })
      .finally(() => {
        setisLoading(false);
      });
    //if (data == "Optoboard configuration completed") { EnableI2C(); DataSet(this.state.pathToJson, this.state.vtrxVersionValue, this.state.inDBCheckBox) };
    //});.catch(err => {this.setState({isLoading: false});});
  };
  const stringLPGBT = <h4 style={{ color: "grey" }}>lpgbt to configure</h4>;
  const stringGBCR = <h4 style={{ color: "grey" }}> gbcr to configure</h4>;

  useEffect(() => {
    const stringLPGBT = switchState ? (
      <h4>lpgbt to configure</h4>
    ) : (
      <h4 style={{ color: "grey" }}>lpgbt to configure</h4>
    );
    const stringGBCR = switchState ? (
      <h4> gbcr to configure</h4>
    ) : (
      <h4 style={{ color: "grey" }}> gbcr to configure</h4>
    );
  }, [switchState]);

  return (
    <React.Fragment>
      <Panel variant="">
        <PanelMain>
          <Bullseye>
            <Button
              spinnerAriaValueText={isLoading ? "Loading" : undefined}
              isLoading={isLoading}
              id="readCharacteristics"
              variant={buttonVariant}
              onClick={(event) => {
                configure(url);
              }}
            >
              {isLoading ? "Configuring..." : "Configure"}
            </Button>
          </Bullseye>

          <Bullseye>
            <Flex>
              <FlexItem>
                <Bullseye>
                  <h3>Manual selection</h3>
                </Bullseye>
              </FlexItem>
              <FlexItem>
                <Switch
                  aria-label="Message when on"
                  //labelOff="Message when off"
                  isChecked={switchState}
                  onChange={() => setswitchState(!switchState)}
                  //isReversed
                />
              </FlexItem>
            </Flex>
          </Bullseye>

          <Bullseye>
            <Flex>
              <FlexItem>{stringLPGBT}</FlexItem>
              <FlexItem>
                <Bullseye>
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_lpgbt1}
                    onChange={() => {
                      toggleBox_lpgbt1(!CheckBox_lpgbt1);
                    }}
                    id={"lpgbt_config_1"}
                    name={"lpgbt_config_1"}
                    isDisabled={!(switchState && optoComponents.lpgbt1)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_lpgbt2}
                    onChange={() => {
                      toggleBox_lpgbt2(!CheckBox_lpgbt2);
                    }}
                    id={"lpgbt_config_2"}
                    name={"lpgbt_config_2"}
                    isDisabled={!(switchState && optoComponents.lpgbt2)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_lpgbt3}
                    onChange={() => toggleBox_lpgbt3(!CheckBox_lpgbt3)}
                    id={"lpgbt_config_3"}
                    name={"lpgbt_config_3"}
                    isDisabled={!(switchState && optoComponents.lpgbt3)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_lpgbt4}
                    onChange={() => toggleBox_lpgbt4(!CheckBox_lpgbt4)}
                    id={"lpgbt_config_4"}
                    name={"lpgbt_config_4"}
                    isDisabled={!(switchState && optoComponents.lpgbt4)}
                  />
                </Bullseye>
              </FlexItem>
            </Flex>
          </Bullseye>

          <Bullseye>
            <Flex>
              <FlexItem>{stringGBCR}</FlexItem>
              <FlexItem>
                <Bullseye>
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_gbcr1}
                    onChange={() => toggleBox_gbcr1(!CheckBox_gbcr1)}
                    id={"gbcr_config_1"}
                    name={"gbcr_config_1"}
                    isDisabled={!(switchState && optoComponents.gbcr1)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_gbcr2}
                    onChange={() => toggleBox_gbcr2(!CheckBox_gbcr2)}
                    id={"gbcr_config_2"}
                    name={"gbcr_config_2"}
                    isDisabled={!(switchState && optoComponents.gbcr2)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_gbcr3}
                    onChange={() => toggleBox_gbcr3(!CheckBox_gbcr3)}
                    id={"gbcr_config_3"}
                    name={"gbcr_config_3"}
                    isDisabled={!(switchState && optoComponents.gbcr3)}
                  />
                  <Checkbox
                    label={""}
                    isChecked={CheckBox_gbcr4}
                    onChange={() => toggleBox_gbcr4(!CheckBox_gbcr4)}
                    id={"gbcr_config_4"}
                    name={"gbcr_config_4"}
                    isDisabled={!(switchState && optoComponents.gbcr4)}
                  />
                </Bullseye>
              </FlexItem>
            </Flex>
          </Bullseye>
        </PanelMain>
      </Panel>
    </React.Fragment>
  );
}

export { Config };
