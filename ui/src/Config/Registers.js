const lpgbtv0Write = {};

const lpgbtv1Write = {
  EPRXDLLCONFIG: {
    values: [0, 255],
    subregisters: [
      { EPRXDLLCURRENT: [0, 3] },
      { EPRXDLLCONFIRMCOUNT: [0, 3] },
      { EPRXDLLFSMCLKALWAYSON: [0, 1] },
      { EPRXDLLCOARSELOCKDETECTION: [0, 1] },
      { EPRXENABLEREINIT: [0, 1] },
      { EPRXDATAGATINGDISABLE: [0, 1] },
    ],
  },
  LDCONFIGH: {
    values: [0, 255],
    subregisters: [
      { LDEMPHASISENABLE: [0, 1] },
      { LDMODULATIONCURRENT: [0, 127] },
    ],
  },
  EPRX0CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX03ENABLE: [0, 1] },
      { EPRX02ENABLE: [0, 1] },
      { EPRX01ENABLE: [0, 1] },
      { EPRX00ENABLE: [0, 1] },
      { EPRX0DATARATE: [0, 3] },
      { EPRX0TRACKMODE: [0, 3] },
    ],
  },
  EPRX00CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX00PHASESELECT: [0, 15] },
      { EPRX00INVERT: [0, 1] },
      { EPRX00ACBIAS: [0, 1] },
      { EPRX00TERM: [0, 1] },
      { EPRX00EQ: [0, 1] },
    ],
  },
  EPRX1CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX13ENABLE: [0, 1] },
      { EPRX12ENABLE: [0, 1] },
      { EPRX11ENABLE: [0, 1] },
      { EPRX10ENABLE: [0, 1] },
      { EPRX1DATARATE: [0, 3] },
      { EPRX1TRACKMODE: [0, 3] },
    ],
  },
  EPRX10CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX10PHASESELECT: [0, 15] },
      { EPRX10INVERT: [0, 1] },
      { EPRX10ACBIAS: [0, 1] },
      { EPRX10TERM: [0, 1] },
      { EPRX10EQ: [0, 1] },
    ],
  },
  EPRX2CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX23ENABLE: [0, 1] },
      { EPRX22ENABLE: [0, 1] },
      { EPRX21ENABLE: [0, 1] },
      { EPRX20ENABLE: [0, 1] },
      { EPRX2DATARATE: [0, 3] },
      { EPRX2TRACKMODE: [0, 3] },
    ],
  },
  EPRX20CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX20PHASESELECT: [0, 15] },
      { EPRX20INVERT: [0, 1] },
      { EPRX20ACBIAS: [0, 1] },
      { EPRX20TERM: [0, 1] },
      { EPRX20EQ: [0, 1] },
    ],
  },
  EPRX3CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX33ENABLE: [0, 1] },
      { EPRX32ENABLE: [0, 1] },
      { EPRX31ENABLE: [0, 1] },
      { EPRX30ENABLE: [0, 1] },
      { EPRX3DATARATE: [0, 3] },
      { EPRX3TRACKMODE: [0, 3] },
    ],
  },
  EPRX30CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX30PHASESELECT: [0, 15] },
      { EPRX30INVERT: [0, 1] },
      { EPRX30ACBIAS: [0, 1] },
      { EPRX30TERM: [0, 1] },
      { EPRX30EQ: [0, 1] },
    ],
  },
  EPRX4CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX43ENABLE: [0, 1] },
      { EPRX42ENABLE: [0, 1] },
      { EPRX41ENABLE: [0, 1] },
      { EPRX40ENABLE: [0, 1] },
      { EPRX4DATARATE: [0, 3] },
      { EPRX4TRACKMODE: [0, 3] },
    ],
  },
  EPRX40CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX40PHASESELECT: [0, 15] },
      { EPRX40INVERT: [0, 1] },
      { EPRX40ACBIAS: [0, 1] },
      { EPRX40TERM: [0, 1] },
      { EPRX40EQ: [0, 1] },
    ],
  },
  EPRX5CONTROL: {
    values: [0, 255],
    subregisters: [
      { EPRX53ENABLE: [0, 1] },
      { EPRX52ENABLE: [0, 1] },
      { EPRX51ENABLE: [0, 1] },
      { EPRX50ENABLE: [0, 1] },
      { EPRX5DATARATE: [0, 3] },
      { EPRX5TRACKMODE: [0, 3] },
    ],
  },
  EPRX50CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPRX50PHASESELECT: [0, 15] },
      { EPRX50INVERT: [0, 1] },
      { EPRX50ACBIAS: [0, 1] },
      { EPRX50TERM: [0, 1] },
      { EPRX50EQ: [0, 1] },
    ],
  },
  EPTXDATARATE: {
    values: [0, 255],
    subregisters: [
      { EPTX0DATARATE: [0, 3] },
      { EPTX1DATARATE: [0, 3] },
      { EPTX2DATARATE: [0, 3] },
      { EPTX3DATARATE: [0, 3] },
    ],
  },
  EPTX10ENABLE: {
    values: [0, 255],
    subregisters: [
      { EPTX13ENABLE: [0, 1] },
      { EPTX12ENABLE: [0, 1] },
      { EPTX11ENABLE: [0, 1] },
      { EPTX10ENABLE: [0, 1] },
      { EPTX03ENABLE: [0, 1] },
      { EPTX02ENABLE: [0, 1] },
      { EPTX01ENABLE: [0, 1] },
      { EPTX00ENABLE: [0, 1] },
    ],
  },
  EPTX32ENABLE: {
    values: [0, 255],
    subregisters: [
      { EPTX33ENABLE: [0, 1] },
      { EPTX32ENABLE: [0, 1] },
      { EPTX31ENABLE: [0, 1] },
      { EPTX30ENABLE: [0, 1] },
      { EPTX23ENABLE: [0, 1] },
      { EPTX22ENABLE: [0, 1] },
      { EPTX21ENABLE: [0, 1] },
      { EPTX20ENABLE: [0, 1] },
    ],
  },
  EPTX00CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX00PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX00PREEMPHASISMODE: [0, 3] },
      { EPTX00DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX01_00CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX01INVERT: [0, 1] },
      { EPTX01PREEMPHASISWIDTH: [0, 7] },
      { EPTX00INVERT: [0, 1] },
      { EPTX00PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX02CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX02PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX02PREEMPHASISMODE: [0, 3] },
      { EPTX02DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX03_02CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX03INVERT: [0, 1] },
      { EPTX03PREEMPHASISWIDTH: [0, 7] },
      { EPTX02INVERT: [0, 1] },
      { EPTX02PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX10CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX10PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX10PREEMPHASISMODE: [0, 3] },
      { EPTX10DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX11_10CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX11INVERT: [0, 1] },
      { EPTX11PREEMPHASISWIDTH: [0, 7] },
      { EPTX10INVERT: [0, 1] },
      { EPTX10PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX12CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX12PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX12PREEMPHASISMODE: [0, 3] },
      { EPTX12DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX13_12CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX13INVERT: [0, 1] },
      { EPTX13PREEMPHASISWIDTH: [0, 7] },
      { EPTX12INVERT: [0, 1] },
      { EPTX12PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX20CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX20PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX20PREEMPHASISMODE: [0, 3] },
      { EPTX20DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX21_20CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX21INVERT: [0, 1] },
      { EPTX21PREEMPHASISWIDTH: [0, 7] },
      { EPTX20INVERT: [0, 1] },
      { EPTX20PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX22CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX22PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX22PREEMPHASISMODE: [0, 3] },
      { EPTX22DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX23_22CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX23INVERT: [0, 1] },
      { EPTX23PREEMPHASISWIDTH: [0, 7] },
      { EPTX22INVERT: [0, 1] },
      { EPTX22PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX30CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX30PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX30PREEMPHASISMODE: [0, 3] },
      { EPTX30DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX31_30CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX31INVERT: [0, 1] },
      { EPTX31PREEMPHASISWIDTH: [0, 7] },
      { EPTX30INVERT: [0, 1] },
      { EPTX30PREEMPHASISWIDTH: [0, 7] },
    ],
  },
  EPTX32CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX32PREEMPHASISSTRENGTH: [0, 7] },
      { EPTX32PREEMPHASISMODE: [0, 3] },
      { EPTX32DRIVESTRENGTH: [0, 7] },
    ],
  },
  EPTX33_32CHNCNTR: {
    values: [0, 255],
    subregisters: [
      { EPTX33INVERT: [0, 1] },
      { EPTX33PREEMPHASISWIDTH: [0, 7] },
      { EPTX32INVERT: [0, 1] },
      { EPTX32PREEMPHASISWIDTH: [0, 7] },
    ],
  },
};

const lpgbtv0Read = {
  ...lpgbtv0Write,
  ...{
    ROM: {
      values: [],
      subregisters: [{ ROMREG: [] }],
    },
  },
};

const lpgbtv1Read = {
  ...lpgbtv1Write,
  ...{
    ROM: {
      values: [],
      subregisters: [{ ROMREG: [] }],
    },
  },
};

const gbcrv2 = {
  CH1UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH1DISEQLF: [0, 1] },
      { CH1EQATT: [0, 3] },
      { CH1CMLAMPLSEL: [0, 7] },
    ],
  },
  CH1UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH1CTLEHFSR: [0, 15] }, { CH1CTLEHFSR: [0, 15] }],
  },
  CH1UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH1DIS: [0, 1] },
      { CH1DISDFF: [0, 1] },
      { CH1DISLPF: [0, 1] },
    ],
  },
  CH2UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH2DISEQLF: [0, 1] },
      { CH2EQATT: [0, 3] },
      { CH2CMLAMPLSEL: [0, 7] },
    ],
  },
  CH2UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH2CTLEHFSR: [0, 15] }, { CH2CTLEHFSR: [0, 15] }],
  },
  CH2UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH2DIS: [0, 1] },
      { CH2DISDFF: [0, 1] },
      { CH2DISLPF: [0, 1] },
    ],
  },
  CH3UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH3DISEQLF: [0, 1] },
      { CH3EQATT: [0, 3] },
      { CH3CMLAMPLSEL: [0, 7] },
    ],
  },
  CH3UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH3CTLEHFSR: [0, 15] }, { CH3CTLEHFSR: [0, 15] }],
  },
  CH3UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH3DIS: [0, 1] },
      { CH3DISDFF: [0, 1] },
      { CH3DISLPF: [0, 1] },
    ],
  },
  CH4UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH4DISEQLF: [0, 1] },
      { CH4EQATT: [0, 3] },
      { CH4CMLAMPLSEL: [0, 7] },
    ],
  },
  CH4UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH4CTLEHFSR: [0, 15] }, { CH4CTLEHFSR: [0, 15] }],
  },
  CH4UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH4DIS: [0, 1] },
      { CH4DISDFF: [0, 1] },
      { CH4DISLPF: [0, 1] },
    ],
  },
  CH5UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH5DISEQLF: [0, 1] },
      { CH5EQATT: [0, 3] },
      { CH5CMLAMPLSEL: [0, 7] },
    ],
  },
  CH5UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH5CTLEHFSR: [0, 15] }, { CH5CTLEHFSR: [0, 15] }],
  },
  CH5UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH5DIS: [0, 1] },
      { CH5DISDFF: [0, 1] },
      { CH5DISLPF: [0, 1] },
    ],
  },
  CH6UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH6DISEQLF: [0, 1] },
      { CH6EQATT: [0, 3] },
      { CH6CMLAMPLSEL: [0, 7] },
    ],
  },
  CH6UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH6CTLEHFSR: [0, 15] }, { CH6CTLEHFSR: [0, 15] }],
  },
  CH6UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH6DIS: [0, 1] },
      { CH6DISDFF: [0, 1] },
      { CH6DISLPF: [0, 1] },
    ],
  },
  CH7UPLINK0: {
    values: [0, 63],
    subregisters: [
      { CH7DISEQLF: [0, 1] },
      { CH7EQATT: [0, 3] },
      { CH7CMLAMPLSEL: [0, 7] },
    ],
  },
  CH7UPLINK1: {
    values: [0, 255],
    subregisters: [{ CH7CTLEHFSR: [0, 15] }, { CH7CTLEHFSR: [0, 15] }],
  },
  CH7UPLINK2: {
    values: [0, 7],
    subregisters: [
      { CH7DIS: [0, 1] },
      { CH7DISDFF: [0, 1] },
      { CH7DISLPF: [0, 1] },
    ],
  },
  CH1DOWNLINK0: {
    values: [0, 63],
    subregisters: [
      { TX1DLATT: [0, 3] },
      { TX1DISDLEMP: [0, 1] },
      { TX1DLSR: [0, 7] },
    ],
  },
  CH1DOWNLINK1: {
    values: [0, 3],
    subregisters: [{ TX1DISDLBIAS: [0, 1] }, { TX1DISDLLPFBIAS: [0, 1] }],
  },
  CH2DOWNLINK0: {
    values: [0, 63],
    subregisters: [
      { TX2DLATT: [0, 3] },
      { TX2DISDLEMP: [0, 1] },
      { TX2DLSR: [0, 7] },
    ],
  },
  CH2DOWNLINK1: {
    values: [0, 3],
    subregisters: [{ TX2DISDLBIAS: [0, 1] }, { TX2DISDLLPFBIAS: [0, 1] }],
  },
  PHASESHIFTER0: {
    values: [0, 3],
    subregisters: [{ DLLENABLE: [0, 1] }, { DLLCAPRESET: [0, 1] }],
  },
  PHASESHIFTER1: {
    values: [0, 31],
    subregisters: [{ DLLFORCEDOWN: [0, 1] }, { DLLCHARGEPUMPCURRENT: [0, 15] }],
  },
  PHASESHIFTER2: {
    values: [0, 255],
    subregisters: [
      { DLLCLOCKDELAYCH7: [0, 15] },
      { DLLCLOCKDELAYCH6: [0, 15] },
    ],
  },
  PHASESHIFTER3: {
    values: [0, 255],
    subregisters: [
      { DLLCLOCKDELAYCH5: [0, 15] },
      { DLLCLOCKDELAYCH4: [0, 15] },
    ],
  },
  PHASESHIFTER4: {
    values: [0, 255],
    subregisters: [
      { DLLCLOCKDELAYCH3: [0, 15] },
      { DLLCLOCKDELAYCH2: [0, 15] },
    ],
  },
  PHASESHIFTER5: {
    values: [0, 255],
    subregisters: [
      { DLLCLOCKDELAYCH1: [0, 15] },
      { DLLCLOCKDELAYCHTEST: [0, 15] },
    ],
  },
  LVDSRXTX: {
    values: [0, 127],
    subregisters: [
      { RXEN: [0, 1] },
      { RXSETCM: [0, 1] },
      { RXENTERMINATION: [0, 1] },
      { RXINVDATA: [0, 1] },
      { RXEQ: [0, 3] },
      { DISTX: [0, 1] },
    ],
  },
};

const gbcrv3 = {
  CH1UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH1DIS: [0, 1]},
      {CH1DISLPF: [0, 1]},
      {CH1MUX: [0,31]}
    ]
  },
  CH1UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH1CTLEHF1SR: [0, 15]},
      {CH1CTLEHF2SR: [0, 15]}
    ]
  },
  CH1UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH1CTLEHF3SR: [0, 15]},
      {CH1CTLEMFSR: [0, 15]}
    ]
  },
  CH1UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH1DISEQLF: [0, 1]},
      {CH1CMLAMPLSEL: [0, 7]},
      {CH1CLKDELAY: [0, 15]}
    ]
  },
  CH2UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH2DIS: [0, 1]},
      {CH2DISLPF: [0, 1]},
      {CH2MUX: [0,31]}
    ]
  },
  CH2UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH2CTLEHF1SR: [0, 15]},
      {CH2CTLEHF2SR: [0, 15]}
    ]
  },
  CH2UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH2CTLEHF3SR: [0, 15]},
      {CH2CTLEMFSR: [0, 15]}
    ]
  },
  CH2UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH2DISEQLF: [0, 1]},
      {CH2CMLAMPLSEL: [0, 7]},
      {CH2CLKDELAY: [0, 15]}
    ]
  },
  CH3UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH3DIS: [0, 1]},
      {CH3DISLPF: [0, 1]},
      {CH3MUX: [0,31]}
    ]
  },
  CH3UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH3CTLEHF1SR: [0, 15]},
      {CH3CTLEHF2SR: [0, 15]}
    ]
  },
  CH3UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH3CTLEHF3SR: [0, 15]},
      {CH3CTLEMFSR: [0, 15]}
    ]
  },
  CH3UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH3DISEQLF: [0, 1]},
      {CH3CMLAMPLSEL: [0, 7]},
      {CH3CLKDELAY: [0, 15]}
    ]
  },
  CH4UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH4DIS: [0, 1]},
      {CH4DISLPF: [0, 1]},
      {CH4MUX: [0,31]}
    ]
  },
  CH4UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH4CTLEHF1SR: [0, 15]},
      {CH4CTLEHF2SR: [0, 15]}
    ]
  },
  CH4UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH4CTLEHF3SR: [0, 15]},
      {CH4CTLEMFSR: [0, 15]}
    ]
  },
  CH4UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH4DISEQLF: [0, 1]},
      {CH4CMLAMPLSEL: [0, 7]},
      {CH4CLKDELAY: [0, 15]}
    ]
  },
  CH5UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH5DIS: [0, 1]},
      {CH5DISLPF: [0, 1]},
      {CH5MUX: [0,31]}
    ]
  },
  CH5UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH5CTLEHF1SR: [0, 15]},
      {CH5CTLEHF2SR: [0, 15]}
    ]
  },
  CH5UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH5CTLEHF3SR: [0, 15]},
      {CH5CTLEMFSR: [0, 15]}
    ]
  },
  CH5UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH5DISEQLF: [0, 1]},
      {CH5CMLAMPLSEL: [0, 7]},
      {CH5CLKDELAY: [0, 15]}
    ]
  },
  CH6UPLINK0: {
    values: [0, 127],
    subregisters: [
      {CH6DIS: [0, 1]},
      {CH6DISLPF: [0, 1]},
      {CH6MUX: [0,31]}
    ]
  },
  CH6UPLINK1: {
    values: [0, 63],
    subregisters: [
      {CH6CTLEHF1SR: [0, 15]},
      {CH6CTLEHF2SR: [0, 15]}
    ]
  },
  CH6UPLINK2: {
    values: [0, 63],
    subregisters: [
      {CH6CTLEHF3SR: [0, 15]},
      {CH6CTLEMFSR: [0, 15]}
    ]
  },
  CH6UPLINK3: {
    values: [0, 15],
    subregisters: [
      {CH6DISEQLF: [0, 1]},
      {CH6CMLAMPLSEL: [0, 7]},
      {CH6CLKDELAY: [0, 15]}
    ]
  },
  CH1DOWNLINK0: {
    values: [0, 63],
    subregisters: [
      {TX1SC2: [0, 15]},
      {TX1SC1: [0, 15]}
    ]
  },
  CH1DOWNLINK1: {
    values: [0, 63],
    subregisters: [
      {TX1AMP: [0, 7]},
      {TX1SR1: [0, 31]}
    ]
  },
  CH1DOWNLINK2: {
    values: [0, 127],
    subregisters: [
      {TX1SR2: [0, 31]},
      {TX1DISPREEMPH: [0, 1]},
      {TX1DISBIAS: [0, 1]}
    ]
  },
  CH2DOWNLINK0: {
    values: [0, 63],
    subregisters: [
      {TX2SC2: [0, 15]},
      {TX2SC1: [0, 15]}
    ]
  },
  CH2DOWNLINK1: {
    values: [0, 63],
    subregisters: [
      {TX2AMP: [0, 7]},
      {TX2SR1: [0, 31]}
    ]
  },
  CH2DOWNLINK2: {
    values: [0, 127],
    subregisters: [
      {TX2SR2: [0, 31]},
      {TX2DISPREEMPH: [0, 1]},
      {TX2DISBIAS: [0, 1]}
    ]
  },
  PHASESHIFTER0: {
    values: [0, 31],
    subregisters: [
      {CLKRXENABLE: [0, 1]},
      {CLKTXDELAY: [0, 15]},
      {DISCLKTX: [0, 1]}
    ]
  },
  PHASESHIFTER1: {
    values: [0, 255],
    subregisters: [
      {DLLCHARGEPUMPCURRENT: [0, 15]},
      {DLLFORCEDOWN: [0, 1]},
      {DLLENABLE: [0, 1]},
      {DLLCAPRESET: [0, 1]}
    ]
  }
};

const vtrxv12 = {
  C0CR: {
    subregisters: [
      { C0FEP: [0, 1] },
      { C0REP: [0, 1] },
      { C0MEN: [0, 1] },
      { C0BEN: [0, 1] },
      { C0LAEN: [0, 1] },
      { C0CEN: [0, 1] },
    ],
    values: [0, 63],
  },
  C1CR: {
    subregisters: [
      { C1FEP: [0, 1] },
      { C1REP: [0, 1] },
      { C1MEN: [0, 1] },
      { C1BEN: [0, 1] },
      { C1LAEN: [0, 1] },
      { C1CEN: [0, 1] },
    ],
    values: [0, 63],
  },
  C2CR: {
    subregisters: [
      { C2FEP: [0, 1] },
      { C2REP: [0, 1] },
      { C2MEN: [0, 1] },
      { C2BEN: [0, 1] },
      { C2LAEN: [0, 1] },
      { C2CEN: [0, 1] },
    ],
    values: [0, 63],
  },
  C3CR: {
    subregisters: [
      { C3FEP: [0, 1] },
      { C3REP: [0, 1] },
      { C3MEN: [0, 1] },
      { C3BEN: [0, 1] },
      { C3LAEN: [0, 1] },
      { C3CEN: [0, 1] },
    ],
    values: [0, 63],
  },
};

const vtrxv13 = {
  GCR: {
    subregisters: [
      { CH1EN: [0, 1] },
      { CH2EN: [0, 1] },
      { CH3EN: [0, 1] },
      { CH4EN: [0, 1] },
    ],
    values: [0, 15],
  },
};

export const Registers = {
  lpgbtv0Write: lpgbtv0Write,
  lpgbtv1Write: lpgbtv1Write,
  lpgbtv0Read: lpgbtv0Read,
  lpgbtv1Read: lpgbtv1Read,
  gbcrv2: gbcrv2,
  gbcrv3: gbcrv3,
  vtrxv12: vtrxv12,
  vtrxv13: vtrxv13,
};
