# ITk-demo-Optoboard example

## Installation

for the Optoboard stack no installation is needed apart from Docker

configure the stack

```shell
./config
```

(check the values set to the .env file are correct)

start the stack

```shell
docker compose up
```

The API endpoints will be available on port 5009 and the UI on port 5089.

## Standalone usage of Optoboard API

At the start of the API container, the optoboard instances are automatically initialized inside the container (this is also done automatically by the UI during the initial loading). The function will read the configuration file located in the mounted volume from
```shell
/opto-ui-config/ConfigGUI.json
```
to
```shell
/config/ConfigGUI.json
```
In case the user changes the local `/opto-ui-config/ConfigGUI.json`, the changes can be loaded in the API container curling the `http://$(hostname):5009/loadOptoList` endpoint.
