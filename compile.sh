#!/usr/bin/env bash
NVM_VERSION=16.13.2

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install ${NVM_VERSION}
nvm use ${NVM_VERSION}

cd ui

npm install
npm run build 

cd ..

