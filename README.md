# `itk-demo-optoboard` Containerized optoboard_felix package

## Overview

Containerized wrapper for optoboard_felix package.

It consists of three OCI images needed to run the API and an image for the UI:

1. **[`itk-demo-optoboard-api`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard/itk-demo-optoboard-api:latest)**
   - Image based on `gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/optoboard-container/optoboard-container:latest`: optoboard_felix is installed as a python package
   - Implements [s6-overlay](https://github.com/just-containers/s6-overlay) for process management (WIP)
   - Provides a Flask API server for remote control served by `gunicorn`.
2. **[`itk-demo-optoboard-worker`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard/itk-demo-optoboard-api:latest)**
   - Based on the same image used for the api
   - Starts a celery worker, used to queue the job requests received by the Flask server
3. **[`itk-demo-optoboard-rabbitmq`]**
   - Runs RabbitMQ
4. **[`itk-demo-optoboard-ui`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard/itk-demo-optoboard-ui:latest)**
   Browser based UI.

Orchestration with the other microservices is possible using the [service registry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/service-registry). More documentation is available at https://atlas-itk-pixel-systemtest.docs.cern.ch/ and https://demi.docs.cern.ch/.

There are 2 suggested ways of operating the Optoboard microservices in combination with the other required microservices:

1. Copy the example folder locally, source the `config` file and run `docker compose up`
2. Use a deployment stack, following the model of the [Bern stack](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/bern)

More information is given in the [software documentation](https://optoboard-system.docs.cern.ch/software/).

## User Instructions

### Prerequisites

- [Working Docker installation](https://demi.docs.cern.ch/containers/docker/) with `docker compose` plug-in (or compatible runtime / compose tool).

The prerequisites can be checked with the `./bootstrap` script.


### Run natively

Clone git repository and enter it

```
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard.git
cd itk-demo-optoboard
```

Check the dependencies

```
./bootstrap       # check dependencies
```

Setup the environment variables

```
./config          # write configured instance(s)
```

To bring up the containers, use one of the following methods.

### Method 1: Start the containers including felix backend and service registry

The simplest method to use the containers is via a deployment orchestration stack.
```
docker compose up # pull UI, API, FELIX Reflex Server and service registry images and start them
```
### Method 2: Start the `API` and `UI` containers separately
```
cd api/
docker compose up # pull API image and start API
```

Now open a new terminal in `itk-demo-optoboard` and 

```
./config  
cd ../ui/
docker compose up # pull UI image and start UI
```

The API should be available at `http://hostname:5009` and the UI at `http://hostname:5089`. To check the ports on which the services are available, run

```
docker ps
```

Should you have any problems/comments on the Optoboard GUI contents ask dal.santo.daniele@cern.ch. 

## Use of FELIX 

The suggested way to run felix is via the [itk-demo-felix](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/tree/main/) microservice, that automatically runs felix-core or felix-star, depending on the value of a specific environment variable.

## Use of the configdb

One can use the configdb to store the configuration files and the tests. For more information about the structure of the configdb containers, look at [itk-demo-configdb](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb) . 
At the first use, one can import a runkey from the local directory onto the backend config database with the following command in the pyconfgdb container shell:
```
pyconfigdb import-rk workspace/Basic basic --commit
``` 
Note that this will import the runkey Basic located in "example/configs/import" with the name "basic". One need to adapt this runkey for its own setup.
One can also manage the runkeys directly in the runkey-UI container and website.

If one wants to use the configdb, it has to set the variable:
```
- OB_USE_CONFIGDB=1       # in itk-demo-optoboard-worker AND itk-demo-optoboard-ui
```
If the configdb was changed by adding or changing a runkey, the optoboard GUI webpage needs to be reloaded.

## Info for developers

- Devcontainers are practical if you are working in an environment that does not have all required development dependencies installed locally. Devcontainers work easiest with the VSCode editor where they can be built on the fly and accessed in the built-in terminal. The suggested way to develop the `UI` is by bringing up the stack (using one of the methods shown above) and then opening this repository with the devcontainer: after the creation of the container, the required node packages will be automatically installed in the `./ui` folder with `npm install` and a development server implementing hot reloading will be created with `npm run start` and bound to `http://${HOST}:3000`
- The flask server can be started by executing the `wsgi.py` script
- The SwaggerUI is available at `http://${HOST}:5009/ui`

## Changing configuration files
When a configuration file is modified or changed completely, run the ```getNewConfig``` function.
