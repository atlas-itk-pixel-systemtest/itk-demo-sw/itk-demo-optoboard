#!/bin/bash

touch /etc/ld.so.conf.d/atlas.conf
for i in $(dirname $(find /sw/atlas -name "*.so*") | sort |uniq); do
  echo $i >> /etc/ld.so.conf.d/atlas.conf
done
for i in $(dirname $(find /opt/lcg -name "*.so*") | sort |uniq); do 
  echo $i >> /etc/ld.so.conf.d/atlas.conf
done
ldconfig
