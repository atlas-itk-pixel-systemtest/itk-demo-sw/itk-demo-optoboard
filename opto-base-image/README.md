# Optoboard Docker Container

This package provides scripts to create a docker container
for the [optoboard_felix](https://gitlab.cern.ch/bat/optoboard_felix) configuration tools.

## Option 1: Running the Optoboard Docker Container and the FELIX Reflex Server

**The suggested procedure is to start the Optoboard container together with the FELIX Reflex Server**, in order to have all the functionalities available. In this way `felixcore` is automatically started.

### Start the containers with docker compose

Download the `docker-compose-optostack.yml` file and 

```shell
docker compose -f docker-compose-optostack.yml up
```

## Using InitOpto in the container

### Method 1: From the container shell
After attaching a shell to the container ([instructions on how to attach a shell to a running container](https://docs.docker.com/engine/reference/commandline/attach/); using the VSCode Docker extension is suggested), it is possible to initialize the optoboard object with eg.

```shell
InitOpto -i -optoboard_serial 33000000 -vtrx_v 1.3 -flx_G 0 -flx_d 0 -configure 1
```

using the appropriate parameters.

### Method 2: Using the InitOpto shim

A shim is available to run `InitOpto` inside the container from the local machine.
To enable it, source the setup script provided in this repository
```shell
. ./setup
```
Then you will have access to `InitOpto` as if it was installed locally, eg.:

```shell
InitOpto -i -optoboard_serial 33000000 -vtrx_v 1.3 -flx_G 0 -flx_d 0 -configure 1
```

## Option 2: Running the Optoboard Docker Container (stand-alone)

There are various methods to obtain a shell inside the container with optoboard_felix and dependencies installed. This procedure only starts the `optoboard-container`!

### Method 1: Using the optoboard-container script

Download the `optoboard-container` script (and optionally the `docker-compose.yml`) from this repo and run it:
```shell
./optoboard-container
```

### Method 2: Manually pulling and starting the image

```shell
IMAGE=gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/optoboard-container/optoboard-container:latest
docker pull $IMAGE
docker compose up
```

## Default mounts in the container

The local working directory `$PWD` is mounted at `/workspace`. This is also the default working directory.

Personal configuration files can be mounted in the ```/config``` folder of the container by placing them in ```${PWD}/.docker/config```. 

Any result obtained in the container can be accessed in the local machine via the volume mount ```${PWD}/.docker/results:/results```.

## Build

The [ic-over-netio](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/external/ic-over-netio-From-NSW-Felixcore/-/tree/optostack?ref_type=heads) and [lpgbt-com](https://gitlab.cern.ch/itk-daq-sw/drivers/lpgbt-com/-/tree/devel_mw?ref_type=heads) libraries are installed in the container.

## Local (non-container) build

Scripts to create local installations are provided in `lcg101_rpm/`.
Please see the README there.
