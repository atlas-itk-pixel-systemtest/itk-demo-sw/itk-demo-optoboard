# Changelog

## demi-7.1 tag
*optoboard_felix-1.0.48*
- Organised in stack
- Updated way of accessing the database (hidden to user). The user must give the SR_URL and the CONFIGDB_KEY in the environment variable of the itk-demo-optoboard container
- Package requirement update
## demi-7 tag
*optoboard_felix-1.0.47*
- Compatible with FELIX FW 5.0 and SW 05-00-04
- Increase of configuration speed (~2.5x)
- optoboard.log ownership issue fixed
- New endpoint for eye monitoring diagram with DC offset (or bias) calculation
- Reorganisation of soft error endpoints - now two endpoints for counter and scan that can be used on either single or multiple links
    - In the case of scans, the links must come from the same device
- New way of using the Configdb. Now the config file of each OB must be included as payload.

## demi-6 tag
*optoboard-felix-1.0.43*
- Configdb included in itk-demo-optoboard. Possibility to use it or not.
- New endpoints, including: register configuration dumping function (also implemented on UI side), parallel soft error counter function and lpGBT supply voltage reading function.

## demi-5 tag
*optoboard-felix-1.0.39*
- Improved exception handling in API
- Improved reply from configureAll endpoint

## demi-4 tag
*optoboard-felix-1.0.37*
- Clean-up of API endpoints: many endpoints have now an easier name and we added endpoints to 1-reset the clock to the slave chips, 2-reset the i2c communication
- The connection to the service registry is now through the itk-demo-registry and not to etcd directly: a change in the version of the hook package was needed as well as of the nginx /config endpoint.

## demi-3 tag
*optoboard-felix-1.0.36*

Add support to run containers in bridge mode. 
Relevant for users: the following variable "INTERFACE" controls how the optoboard-worker container connects to the felix server. Notice that, depending on whether felixcore or felixstar is running, INTERFACE needs to be set as following:
- use the name of the optoboard worker container (or its IP address) when using lpgbt-com
- use the name of the felix container (or its IP address) when using ic-over-netio
